<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ManageHolidays extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'manage_holidays';
        
        public function AddManageHolidays ($dataArray){
            $this->Party = $dataArray['party'];
            $this->PartyID = $dataArray['partyid'];
            $this->Type = $dataArray['holidaytype'];
            $this->Title = $dataArray['title'];
            $this->Holiday = $dataArray['holiday'];
            $this->From_Time = $dataArray['fromtime'];   
            $this->To_Time = $dataArray['totime'];     
            $this->Created_on = time();
            $this->created_at = time();
            $this->updated_at = 0;
            $this->Active = 1;

            if($this->save()){
                $insertedId = $this->id;
                return $insertedId;
            }else{
                return false;
            }      
        }
        
        public function FindExistingClinicHolidays($party,$partyid){ 
            $allData = DB::table('manage_holidays')
                ->where('Party', '=', $party)
                ->where('PartyID', '=', $partyid)  
                ->where('Active', '=', 1)    
                ->get();
            return $allData;
        }
        
        public function UpdateManageHolidays($dataArray){       
            $allData = DB::table('manage_holidays')
                ->where('ManageHolidayID', '=', $dataArray['manageholidayid'])
                ->update($dataArray);
            
            return $allData;
        }
        
        public function FindPartyFullDayHolidays($party,$partyid,$holiday){    
            $allData = DB::table('manage_holidays')
                ->where('Party', '=', $party)
                ->where('PartyID', '=', $partyid) 
                ->where('Type', '=', 0) 
                ->where('Holiday', '=', $holiday)    
                ->where('Active', '=', 1)    
                ->first();
            return $allData;
        }
        public function FindCurrentDayHolidays($party,$partyid,$holiday){    
            $allData = DB::table('manage_holidays')
                ->where('Party', '=', $party)
                ->where('PartyID', '=', $partyid) 
                //->where('Type', '=', 0) 
                ->where('Holiday', '=', $holiday)    
                ->where('Active', '=', 1)    
                ->first();
            return $allData;
        }
        
        public function FindUpcomingHolidays($party,$partyid,$holiday){ 
            $allData = DB::table('manage_holidays')
                ->where('Party', '=', $party)
                ->where('PartyID', '=', $partyid)  
                ->where('Holiday', '>=', $holiday)  
                ->where('Active', '=', 1)    
                ->get();
            return $allData;
        }
        
//        public function FindClinic7DayHolidays($party,$partyid,$holiday,$days7){ 
//            $allData = DB::table('manage_holidays')
//                ->where('Party', '=', $party)
//                ->where('PartyID', '=', $partyid)  
//                ->where('Holiday', '>=', $holiday && 'Holiday', '<=', $days7)  
//                ->where('Active', '=', 1)    
//                ->get();
//            return $allData;
//        }
        
        
        
        
        
        
        
        
        
        
}