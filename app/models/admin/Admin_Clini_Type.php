<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Admin_Clinic_Type extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'clinic_types';
	protected $primaryKey = 'ClinicTypeID';

 	
 	//Get all clinic types
	public function GetClinicTypes()
	{
		$clinicTypeData = DB::table('clinic_types')
		    ->where('Active',1)
		    ->lists('Name','ClinicTypeID');
			return $clinicTypeData;
	}
}
