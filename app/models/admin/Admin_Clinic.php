<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Admin_Clinic extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'clinic';
	protected $primaryKey = 'ClinicID';
	public $timestamps = false;

	// public static function validate($input)
	// {
	// 	$rules = array(
	// 			'Name'=>'required',
	// 			'Description'=>'required',
	// 			);

	// 	return Validator::make($input, $rules);
	// }


    // Get all clinic details
    public function GetClinicDetails()
    {
       $clinicData = DB::table('clinic')                                                            
	 			->leftJoin ('doctor_availability', 'clinic.ClinicID', '=', 'doctor_availability.ClinicID')
                ->select('clinic.ClinicID','clinic.Name','clinic.image','clinic.City','clinic.Country','clinic.Lat','clinic.Lng','clinic.Phone','clinic.Opening','clinic.image','clinic.Active',DB::raw('COUNT(medi_doctor_availability.ClinicID) AS DoctorCount'),DB::raw('COUNT(CASE WHEN medi_doctor_availability.Active = 1 THEN medi_doctor_availability.ClinicID END)AS ActiveCount'),DB::raw('COUNT(CASE WHEN medi_doctor_availability.Active = 0 THEN medi_doctor_availability.ClinicID END)AS InactiveCount'))                
                ->groupBy('clinic.ClinicID');
                // ->get();

                return $clinicData;    
    }

    // Add all clinic details
    public function AddClinic()
    {

    	$this->Custom_title	= Input::get('custom_title');                            
    	$this->Name 		= Input::get('name');                            
		$this->Description  = Input::get('description');            
		$this->Website  	= Input::get('website');            
		$this->Address      = Input::get('address');            
		$this->City         = Input::get('city');            
		$this->State        = Input::get('state');            
		$this->Country      = Input::get('country');            
		$this->Postal       = Input::get('postal');            
		$this->District     = Input::get('district');            
		$this->Lat          = Input::get('latitude');  
		$this->Lng          = Input::get('longitude');
		$this->Phone        = Input::get('phone');            
		$this->MRT          = Input::get('mrt');            
		$this->Opening      = Input::get('opening');            
		$this->Created_on   = time();                                                
		$this->Active       = 1; 
		if(Input::hasFile('file'))
		{
			$upload 		= Image_Library::CloudinaryUpload(); // image upload			
    		$this->image 	= $upload;        
		}
		else
		{
            $this->image 	= 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1439208475/medilogo_cn6d0x.png';		
		}

		if(Input::hasFile('clinic_price'))
		{
			$uploadClinicPrice 		= Image_Library::CloudinaryUploadFile(Input::file('clinic_price')); // image upload			
    		$this->Clinic_Price 	= $uploadClinicPrice;        
		}

    	if($this->save()){
    		$clinicId = $this->ClinicID;
    		return $clinicId;
    	}else{
    		return false;
    	}      
    }

    public function UpdateClinic($dataArray)
    { 		
		$allData = DB::table('clinic')
                ->where('ClinicID', '=', $dataArray['clinicid'])
                ->update($dataArray);
            
            return $allData;
    }

    //Get all clinic list
	public function GetClinicList()
	{
		$clinicData = DB::table('clinic')
		    ->where('Active',1)
		    ->lists('Name','ClinicID');
			return $clinicData;
	}
    
        public function GetAllClinics(){
            $getBooking = DB::table('clinic')
                //->where('Name', 'like', "%{$clinicname}%")  
                ->get();
            return $getBooking;
        }

}
