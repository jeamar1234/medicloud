<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
        
        //User login by Mobile app
        public function authLogin ($email, $password){
            
            $users = DB::table('user')
                     ->select('UserID')
                     ->where('Email', '=', $email)
                     ->where('Password', '=', StringHelper::encode($password))
                     ->where('Active', '=', 1)
                     ->first();
            
            if($users){
                //print_r($users);
                return $users->UserID;
            }else{
                return false;
            }  
        }
        
        //User sign up by mobile app
        public function authSignup ($dataArray){
           
                $this->Name = $dataArray['name'];
                $this->Password = $dataArray['password'];
                $this->Email = $dataArray['email'];
                $this->PhoneNo = $dataArray['mobile'];
                //$this->Insurance_Company = $dataArray['insurance_company'];
                //$this->Insurance_Policy_No = $dataArray['insurance_policy_no'];
                $this->Lat = $dataArray['latitude'];
                $this->Lng = $dataArray['longitude'];
                $this->NRIC = $dataArray['nric'];
                $this->FIN = $dataArray['fin'];
                $this->Image = 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1427972951/ls7ipl3y7mmhlukbuz6r.png';
                $this->Active = $dataArray['active']; //to active user
                $this->created_at = $dataArray['createdat'];
                $this->updated_at = 0;
                $this->UserType = $dataArray['usertype']; // for users
			
                if($this->save()){
                    $insertedId = $this->id; 
                    return $insertedId;
                }else{
                    return false;
                }
                
        }
        /*  Access      :   Public
            Function    :   Update 
            Parameter   :   Array
            Author      :   Rizvi
            Return      :   Json
            Updated     :   
        */
        
        public function updateUserProfile ($dataArray){         
            $allData = DB::table('user')
                ->where('UserID', '=', $dataArray['userid'])
                ->update($dataArray);
            
            return $allData;
        }
        
        public function Forgot_Password($email){
            $users = DB::table('user')
                    ->where('Email', '=', $email)
                    ->where('Active', '=', 1)
                     ->first();
            
            if($users){
                return $users->UserID;
            }else{
                return false;
            }  
        }
        
        public function checkEmail ($email){
            
            $findEmail = DB::table('user')
                     ->where('Email', '=', $email)
                    ->where('Active', '=', 1)
                     ->first();
            
            if($findEmail){
                return $findEmail->UserID;
            }else{
                return FALSE;
            }  
        }
        
        //get user profile 
        //parameter : user id 
        //Out put : array 
        public function getUserProfile($profileid){           
            $findUser = DB::table('user')
                    ->where('UserID', '=', $profileid)
                    ->where('Active', '=', 1)
                    ->first();
            
            if($findUser){
                return $findUser;
            }else{
                return FALSE;
            }  
        }
        public function FindOTPCode($profileid,$otpcode){           
            $findUser = DB::table('user')
                    ->where('UserID', '=', $profileid)
                    ->where('OTPCode', '=', $otpcode)
                    ->where('Active', '=', 1)
                    ->first();
               
            return $findUser; 
        }
       public function UserProfileByRef($refid){           
            $findUser = DB::table('user')
                    ->where('Ref_ID', '=', $refid)
                    ->where('Active', '=', 1)
                    ->first();
            
            if($findUser){
                return $findUser;
            }else{
                return FALSE;
            }  
        }
        
        
        
        
        
       //xxxxxxxxxxxxxxxxxxxxxx  For Web xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx//
        
        /* Use          :   Add new users (clinic, doctor and user)
         * Parameter    :
         * Return       :
         */
        public function addNewUser($dataArray){
            $this->Name = $dataArray['name'];
            $this->UserType = $dataArray['usertype'];
            $this->Email = $dataArray['email'];
            $this->PhoneCode = $dataArray['code'];
            $this->PhoneNo = $dataArray['mobile'];
            //$this->Insurance_Company = $dataArray['insurance_company'];
            //$this->Insurance_Policy_No = $dataArray['insurance_policy_no'];
            //$this->Lat = $dataArray['latitude'];
            //$this->Lng = $dataArray['longitude'];
            $this->NRIC = $dataArray['nric'];
            //$this->FIN = $dataArray['fin'];
             
            $this->created_at = time();
            $this->updated_at = 0;
            $this->Ref_ID = $dataArray['ref_id'];
            $this->ActiveLink = $dataArray['activelink'];
            $this->Status = $dataArray['status'];
            $this->Active = 1;
            
            
            
            if($this->save()){
                $insertedId = $this->id;
                return $insertedId;
            }else{
                return false;
            }
        }
        
        /*
         * 
         * 
         */
        public function getUserDetails($profileid){           
            $findUser = DB::table('user')
                    ->where('UserID', '=', $profileid)
                    ->where('Active', '=', 1)
                    ->first();
            
            if($findUser){
                return $findUser;
            }else{
                return FALSE;
            }  
        }
        
        /* Use          :   Used to get details by activation link
         * 
         * 
         */
        public function findDoctorByActivationCode($code){           
            $findUser = DB::table('user')
                    ->where('ActiveLink', '=', $code)
                    ->where('Active', '=', 1)
                    ->where('Status', '=', 0)
                    ->first();
            
            if($findUser){
                return $findUser;
            }else{
                return FALSE;
            }  
        }
        /* Use          :   Used to get details by reset link
         * 
         * 
         */
        public function findDoctorByResetCode($code){           
            $findUser = DB::table('user')
                    ->where('ResetLink', '=', $code)
                    ->where('Active', '=', 1)
                    ->where('Recon', '=', 0)
                    ->first();
            
            if($findUser){
                return $findUser;
            }else{
                return FALSE;
            }  
        }
        
        public function updateUser ($dataArray){         
            $allData = DB::table('user')
                ->where('UserID', '=', $dataArray['userid'])
                ->update($dataArray);
            
            return $allData;
        }
        
        /*
         * 
         * 
         */
        public function checkLogin ($email, $password){
            
            $users = DB::table('user')
                     //->select('UserID')
                     ->where('Email', '=', $email)
                     ->where('Password', '=', StringHelper::encode($password))
                     ->first();
            if($users){
                return $users;
            }else{
                return false;
            }  
        }
        
        /* Use      :   Used to find user details by email 
         * 
         */
        public function checkEmailExist ($email){
            $findEmail = DB::table('user')
                     ->where('Email', '=', $email)
                     ->first();
            if($findEmail){
                return $findEmail;
            }else{
                return FALSE;
            }  
        }
        public function FindUserByNric ($nric){
            $findUser = DB::table('user')
                    ->where('NRIC', '=', $nric)
                    ->where('Active', '=', 1)
                     ->first();
            if($findUser){
                return $findUser;
            }else{
                return FALSE;
            }  
        }
        public function FindRealUser ($nric,$email){
            $findUser = DB::table('user')
                    ->where('NRIC', '=', $nric)
                    ->where('Email', '=', $email)
                    ->where('Active', '=', 1)
                     ->first();
            if($findUser){
                return $findUser;
            }else{
                return FALSE;
            }  
        }

        
        
}
