<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Doctor extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'doctor';

	/**
	 * The attributes excluded from the model's JSON form.
	 * 
	 * @var array
	 */
         
         
	//protected $hidden = array('password', 'remember_token');       
        
        //Add new doctor
        public function insertDoctor ($dataArray){
                $this->Name = $dataArray['name'];
                $this->Email = $dataArray['email'];
                $this->Description = null;
                $this->Qualifications = $dataArray['qualification'];
                $this->Specialty = $dataArray['speciality'];
                $this->Availability = null;
                $this->image = 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1428405297/is9qvklrjvkmts1pvq8r.png';
                $this->Phone = $dataArray['mobile'];
                $this->Emergency = $dataArray['mobile'];
                $this->Created_on = time();
                $this->created_at = time();
                $this->updated_at = 0;
                $this->Active = 1;
			
                if($this->save()){
                    $insertedId = $this->id;
                    return $insertedId;
                }else{
                    return false;
                }      
        }
        public function NewDoctor ($dataArray){
                $this->Name = $dataArray['name'];
                $this->Email = $dataArray['email'];
                //$this->Description = null;
                $this->Qualifications = $dataArray['qualification'];
                $this->Specialty = $dataArray['speciality'];
                //$this->Availability = null;
                $this->image = $dataArray['image'];
                $this->Code = $dataArray['code'];
                $this->Emergency_Code = $dataArray['emergency_code'];
                $this->Phone = $dataArray['phone'];
                $this->Emergency = $dataArray['emergency_phone'];
                $this->Created_on = time();
                $this->created_at = time();
                $this->updated_at = 0;
                $this->Active = 1;
			
                if($this->save()){
                    $insertedId = $this->id;
                    return $insertedId;
                }else{
                    return false;
                }      
        }
        
        public function updateDoctor ($dataArray){
            
            $allData = DB::table('doctor')
                ->where('DoctorID', '=', $dataArray['doctorid'])
                ->update($dataArray);
            
            return $allData;
        }
        // nhr 2016-1-29
        public function updateDoctorByGmail($dataArray){
            // dd($dataArray);
            $allData = DB::table('doctor')
                ->where('gmail', '=', $dataArray['gmail'])
                ->update($dataArray);
            
            return $allData;
        }

        public function findUniqueGmail($gmail){
            $doctorData = DB::table('doctor')
                ->where('gmail', '=', $gmail)
                ->first();
               
            return $doctorData;                
        }
        
        
        // Find a doctor details
        public function doctorDetails($value){
            $doctorData = DB::table('doctor')
                ->where('DoctorID', '=', $value)
                ->where('Active', '=', 1)    
                ->first();
                if($doctorData){
                    return $doctorData;
                }else{
                    return false;
                }    
        }

        public function ClinicDoctors($value){
            $doctorData = DB::table('doctor')
                ->where('DoctorID', '=', $value)
                ->first();
                if($doctorData){
                    return $doctorData;
                }else{
                    return false;
                }    
        }
        /* Use          :   used to find existing doctor by Email
         * Parameter    :   Doctor email
         * Return       :   Doctor details
         * ................................
         * Author       :   Rizvi
         */
        public function findDoctorByEmail($email){
            $doctorData = DB::table('doctor')
                ->where('Email', '=', $email)
                ->first();
               
            return $doctorData;                
        }

       
        
    //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx//
    //                              WEB                                   //
    //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx//

        
    public function FindDoctor($doctorid){
        $doctorData = DB::table('doctor')
            ->where('DoctorID', '=', $doctorid)
            ->first();
                
        return $doctorData;  
    }    
    
    public function FindDoctorDetails($value){
        $doctorData = DB::table('doctor')
            ->where('DoctorID', '=', $value)
            ->where('Active', '=', 1)    
            ->first();
            return $doctorData;   
    }
}
