<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Clinic extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'clinic';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
        
        
	//protected $hidden = array('password', 'remember_token');
        
        public function search ($search){
            $clinicData = DB::table('clinic')    
                    ->select('clinic.ClinicID','clinic.Name as CLName','clinic.Clinic_Type','clinic.Description','clinic.Custom_title','clinic.Website','clinic.image as CLImage','clinic.Address as CLAddress','clinic.State as CLState','clinic.City as CLCity','clinic.Postal as CLPostal','clinic.Phone','clinic.Lat as CLLat','clinic.Lng as CLLng','clinic.Clinic_Price')
                    ->where('Active', '=', 1)
                    //->where('Name', 'LIKE', "%{$search}%")
                    //->orWhere('District', 'LIKE', "%{$search}%")
                    //->orWhere('MRT', 'LIKE', "%{$search}%")
                    
                    ->where(function ($clinicData) use ($search) {
                        $clinicData->where('Name', 'LIKE', "%{$search}%")
                        ->orWhere('District', 'LIKE', "%{$search}%")
                        ->orWhere('MRT', 'LIKE', "%{$search}%");
                    })
                    
                    
                    
                    ->get();
                    
                    /*
                    ->where('Name', 'LIKE', "%{$search}%")
                    ->where('Active', '=', 1)
                    ->orWhere(function($query)
                    {
                        $query->where('District', 'LIKE', "%{$search}%");
                             // ->where('Active', '=', 1);
                    })
                    */
                    
             
            if($clinicData){
                return $clinicData;
            }else{
                return false;
            } 
        }
        
        
        public function ClinicDetails($value){
            $clinicData = DB::table('clinic')
                ->where('ClinicID', '=', $value)
                ->where('Active', '=', 1)    
                ->first();
            
                if($clinicData){
                    return $clinicData;
                }else{
                    return false;
                }    
        }
        
        public function Nearby1($lat,$lng,$radius,$getType){       
            $clinicData = DB::table('clinic')
                ->join('clinic_types_detail', 'clinic.ClinicID', '=', 'clinic_types_detail.ClinicID')
                ->join('clinic_types', 'clinic_types_detail.ClinicTypeID', '=', 'clinic_types.ClinicTypeID')    
                ->select('clinic.ClinicID','clinic.Name','clinic.Address','clinic.image','clinic.Lat','clinic.Lng','clinic.Phone','clinic.Opening','clinic.Clinic_Price',
                        'clinic_types.ClinicTypeID','clinic_types.Name as ClinicType',
                    DB::raw("
                            (3959 * acos( cos( radians({$lat}) ) *
                              cos( radians( lat ) )
                              * cos( radians( lng ) - radians({$lng})
                              ) + sin( radians({$lat}) ) *
                              sin( radians( lat ) ) )
                            ) AS distance"))
                ->where("clinic.Active","=",1)    
                ->where("clinic_types.Active","=",1)  
                ->where("clinic_types_detail.Active","=",1)
                ->where("clinic_types_detail.ClinicTypeID","=",$getType)                      
                ->having("distance", "<", $radius)
                ->orderBy("distance","ASC")
                ->get();
    
            return $clinicData;            
        }
        public function Nearby($lat,$lng,$radius,$getType){       
            $clinicData = DB::table('clinic')
                ->join('clinic_types_detail', 'clinic.ClinicID', '=', 'clinic_types_detail.ClinicID')
                ->join('clinic_types', 'clinic_types_detail.ClinicTypeID', '=', 'clinic_types.ClinicTypeID')    
                ->select('clinic.ClinicID','clinic.Name as CLName','clinic.Clinic_Type','clinic.Description','clinic.Custom_title','clinic.Website','clinic.image as CLImage','clinic.Address as CLAddress','clinic.State as CLState','clinic.City as CLCity','clinic.Postal as CLPostal','clinic.Phone','clinic.Lat as CLLat','clinic.Lng as CLLng','clinic.Clinic_Price',
                        'clinic_types.ClinicTypeID','clinic_types.Name as ClinicType',
                    DB::raw("
                            (3959 * acos( cos( radians({$lat}) ) *
                              cos( radians( lat ) )
                              * cos( radians( lng ) - radians({$lng})
                              ) + sin( radians({$lat}) ) *
                              sin( radians( lat ) ) )
                            ) AS distance"))
                ->where("clinic.Active","=",1)    
                ->where("clinic_types.Active","=",1)  
                ->where("clinic_types_detail.Active","=",1)
                ->where("clinic_types_detail.ClinicTypeID","=",$getType)                      
                ->having("distance", "<", $radius)
                ->orderBy("distance","ASC")
                ->get();
    
            return $clinicData;            
        }
        
        public function Nearby_Old($lat,$lng,$radius){
            
            $clinicData = DB::table('clinic')
                ->select(
                    DB::raw("*,
                            (3959 * acos( cos( radians({$lat}) ) *
                              cos( radians( lat ) )
                              * cos( radians( lng ) - radians({$lng})
                              ) + sin( radians({$lat}) ) *
                              sin( radians( lat ) ) )
                            ) AS distance"))
                ->where("Active","=",1)    
                ->having("distance", "<", $radius)
                ->orderBy("distance","ASC")
                ->get();
            
           if($clinicData){
               return $clinicData;
           }else{
               return false;
           }            
        }
        
        public function PanelClinicNearby($lat,$lng,$radius,$insuranceid){
            
            $clinicData = DB::table('clinic')
                ->join('clinic_insurence_company', 'clinic.ClinicID', '=', 'clinic_insurence_company.ClinicID')
                //->join('insurance_company', 'clinic_insurence_company.InsuranceID', '=', 'insurance_company.CompanyID')      
                //->select('clinic.ClinicID','clinic.Name') 
                ->select(
                    DB::raw("*,
                            (3959 * acos( cos( radians({$lat}) ) *
                              cos( radians( lat ) )
                              * cos( radians( lng ) - radians({$lng})
                              ) + sin( radians({$lat}) ) *
                              sin( radians( lat ) ) )
                            ) AS distance") )
                //->select('clinic.ClinicID')                    
                ->where("clinic.Active","=",1)
                ->where("clinic_insurence_company.Active","=",1)  
                ->where("clinic_insurence_company.InsuranceID","=",$insuranceid)                      
                ->having("distance", "<", $radius)
                ->orderBy("distance","ASC")
                ->get();
            
             //print_r($clinicData);
            return $clinicData;           
        }
        
        
        /*public function Nearby($lat,$lng,$radius){
            
            $clinicData = DB::table('clinic')
                ->select(
                    DB::raw("*,
                            (3959 * acos( cos( radians({$lat}) ) *
                              cos( radians( lat ) )
                              * cos( radians( lng ) - radians({$lng})
                              ) + sin( radians({$lat}) ) *
                              sin( radians( lat ) ) )
                            ) AS distance"))
                ->where("Active","=",1)    
                ->having("distance", "<", $radius)
                ->orderBy("distance","ASC")
                ->get();
            
           if($clinicData){
               return $clinicData;
           }else{
               return false;
           }            
        }*/

     public function FindClinicProfile($clinicid){
         $clinicData = DB::table('clinic')
                ->join('user', 'clinic.ClinicID', '=', 'user.Ref_ID')
                ->select('clinic.ClinicID','clinic.Name as CLName','clinic.Clinic_Type','clinic.Description','clinic.Custom_title','clinic.Website','clinic.image as CLImage','clinic.Address as CLAddress','clinic.State as CLState','clinic.City as CLCity','clinic.Postal as CLPostal','clinic.Phone','clinic.Lat as CLLat','clinic.Lng as CLLng','clinic.Clinic_Price',
                        'user.UserType','user.Email','user.Password')  
                ->where("clinic.Active","=",1)
                ->where("user.Active","=",1)  
                ->where("clinic.ClinicID","=",$clinicid)                      
                ->first();
            
            return $clinicData;
     }

     





     //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx//
        //                              WEB                                   //
        //xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx//
        
        
        
        
        public function FindClinicDetails($value){
            $clinicData = DB::table('clinic')
                ->where('ClinicID', '=', $value)
                ->where('Active', '=', 1)    
                ->first();
   
            return $clinicData;    
        }
        
        public function UpdateClinicDetails($dataArray){         
            $allData = DB::table('clinic')
                ->where('ClinicID', '=', $dataArray['clinicid'])
                ->update($dataArray);
            
            return $allData;
        }
       

}
