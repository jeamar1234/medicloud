/*  xxxxxxxxxxx         Use this page to keep your database upto date     xxxxxxxxxxxxxxxxxxxxxxxx/
*   Use         :       To keep database upto date
*   Created     :       2014-12-11
*   Updated     :       
*   Created by  :       Rizvi
*/



# ================= DB changed on version =========================
# ===================== medicloud_v001 ============================

# New Update
ALTER TABLE `medi_insurance_company` ADD `Annotation_Default` VARCHAR(1500) NULL AFTER `Annotation`;

# Add custom title and webstie to clinic
ALTER TABLE `medi_clinic` ADD `Custom_title` VARCHAR(500) NULL AFTER `Description`, ADD `Website` VARCHAR(256) NULL AFTER `Custom_title`;

# For price update
ALTER TABLE `medi_clinic` ADD `Clinic_Price` VARCHAR(1500) NULL AFTER `MRT`;

# Data update 
INSERT INTO `medi_insurance_company` (`CompanyID`, `Name`, `Description`, `Image`, `Annotation`, `Annotation_Default`, `Active`) VALUES
(111, 'GP', 'GP', 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1428304938/yps120uxtcwuntu5npqi.png', 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1441694118/gp_green_nce5o0.png', 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1441694118/gp_red_zld9ma.png', 1),
(112, 'Dental', 'Dental', 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1428304938/yps120uxtcwuntu5npqi.png', 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1441694117/dentist_green_yurgdc.png', 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1441694118/dentist_red_qj7prg.png', 1),
(113, 'TCM', 'TCM', 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1428304938/yps120uxtcwuntu5npqi.png', 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1441694118/tcm_green_oyk06q.png', 'https://res.cloudinary.com/www-medicloud-sg/image/upload/v1441694118/tcm_red_rn7ck8.png', 1);



# ============= xxxxxxx New Changes Added xxxxxxxx ===============
#                           Medicloud_v002
# ============= xxxxxx                       ===============

# Create Manage Holidays Table
CREATE TABLE `medi_manage_holidays` (
`ManageHolidayID` int(12) NOT NULL,
  `ClinicID` int(12) NOT NULL,
  `DoctorID` int(11) DEFAULT NULL,
  `Party` tinyint(1) DEFAULT NULL COMMENT '3 - Clinic, 2 - Doctor',
  `PartyID` int(12) DEFAULT NULL,
  `Type` tinyint(1) DEFAULT NULL COMMENT '0 - Full, 1 - Custom',
  `Title` varchar(256) DEFAULT NULL,
  `Holiday` varchar(20) DEFAULT NULL,
  `From_Time` varchar(20) DEFAULT NULL,
  `To_Time` varchar(20) DEFAULT NULL,
  `Created_on` int(50) DEFAULT NULL,
  `created_at` int(50) DEFAULT NULL,
  `updated_at` int(50) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Indexes for table `medi_manage_holidays`
--
ALTER TABLE `medi_manage_holidays`
 ADD PRIMARY KEY (`ManageHolidayID`);

--
-- AUTO_INCREMENT for table `medi_manage_holidays`
--
ALTER TABLE `medi_manage_holidays`
MODIFY `ManageHolidayID` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;



# Create Manage Times 
CREATE TABLE `medi_manage_times` (
`ManageTimeID` int(11) NOT NULL,
  `Party` tinyint(1) DEFAULT NULL COMMENT '3 - Clinic, 2 - Doctor',
  `PartyID` int(12) DEFAULT NULL,
  `ClinicID` int(11) DEFAULT NULL,
  `DoctorID` int(12) DEFAULT NULL,
  `Type` tinyint(1) DEFAULT NULL COMMENT '0 - 24, 1 - Custom',
  `From_Date` varchar(50) DEFAULT NULL,
  `To_Date` varchar(50) DEFAULT NULL,
  `Repeat` tinyint(1) DEFAULT NULL COMMENT '0 - No, 1 - Yes',
  `Status` tinyint(1) DEFAULT NULL COMMENT '0 - Inactive, 1 - Active',
  `Created_on` int(50) DEFAULT NULL,
  `created_at` int(50) DEFAULT NULL,
  `updated_at` int(50) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Indexes for table `medi_manage_times`
--
ALTER TABLE `medi_manage_times`
 ADD PRIMARY KEY (`ManageTimeID`);

--
-- AUTO_INCREMENT for table `medi_manage_times`
--
ALTER TABLE `medi_manage_times`
MODIFY `ManageTimeID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;


# Added Manage time Id and Party in Clinic Time
ALTER TABLE `medi_clinic_time` ADD `ManageTimeID` INT(12) NULL AFTER `ClinicTimeID`, ADD `Party` TINYINT(1) NULL AFTER `ManageTimeID`;

# Added Doctor id in Clinic Time
ALTER TABLE `medi_clinic_time` ADD `DoctorID` INT(12) NULL AFTER `ClinicID`;



# Create Clinic Procedures
CREATE TABLE `medi_clinic_procedure` (
`ProcedureID` int(12) NOT NULL,
  `ClinicID` int(12) NOT NULL,
  `Name` varchar(500) DEFAULT NULL,
  `Description` varchar(1500) DEFAULT NULL,
  `Duration` int(7) DEFAULT NULL,
  `Duration_Format` varchar(20) DEFAULT NULL,
  `Price` double(5,2) DEFAULT NULL,
  `Created_on` int(30) DEFAULT NULL,
  `created_at` int(30) DEFAULT NULL,
  `updated_at` int(30) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Indexes for table `medi_clinic_procedure`
--
ALTER TABLE `medi_clinic_procedure`
 ADD PRIMARY KEY (`ProcedureID`);

--
-- AUTO_INCREMENT for table `medi_clinic_procedure`
--
ALTER TABLE `medi_clinic_procedure`
MODIFY `ProcedureID` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;




# Create Doctor Procedures
CREATE TABLE `medi_doctor_procedure` (
`DoctorProcedureID` int(12) NOT NULL,
  `ProcedureID` int(12) NOT NULL,
  `ClinicID` int(12) NOT NULL,
  `DoctorID` int(12) NOT NULL,
  `Created_on` int(30) DEFAULT NULL,
  `created_at` int(30) DEFAULT NULL,
  `updated_at` int(30) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Indexes for table `medi_doctor_procedure`
--
ALTER TABLE `medi_doctor_procedure`
 ADD PRIMARY KEY (`DoctorProcedureID`);

--
-- AUTO_INCREMENT for table `medi_doctor_procedure`
--
ALTER TABLE `medi_doctor_procedure`
MODIFY `DoctorProcedureID` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;

# Add clinic type in medi_clinic 
ALTER TABLE `medi_clinic` ADD `Clinic_Type` INT( 2 ) NULL AFTER `Name` ;


# xxxxxxx Update on 20-12-2015 xxxxxxxxxxxxxx

ALTER TABLE `medi_user_appoinment` ADD `ClinicTimeID` INT( 12 ) NULL AFTER `UserID` ,
ADD `DoctorID` INT( 12 ) NULL AFTER `ClinicTimeID` ,
ADD `ProcedureID` INT( 12 ) NULL AFTER `DoctorID` ,
ADD `StartTime` VARCHAR( 30 ) NULL AFTER `ProcedureID` ,
ADD `EndTime` VARCHAR( 30 ) NULL AFTER `StartTime` ,
ADD `Remarks` VARCHAR( 1000 ) NULL AFTER `EndTime` ;


# Change Procedure price digits 
ALTER TABLE `medi_clinic_procedure` CHANGE `Price` `Price` DOUBLE(7,2) NULL DEFAULT NULL;


# Add phone code on user table 
ALTER TABLE `medi_user` ADD `PhoneCode` VARCHAR(5) NULL AFTER `FIN`;

# change the price format in clinic Procedure 
ALTER TABLE `medi_clinic_procedure` CHANGE `Price` `Price` INT(7) NULL DEFAULT NULL;

-- nhr 29-1-2016
ALTER TABLE `medi_doctor` 
ADD COLUMN  `gmail` VARCHAR(255) NULL COMMENT '' ,
ADD COLUMN  `token` TEXT NULL COMMENT '' ;

-- 2016-2-2
-- // nhr chek google event oe medicloud event gc event=1
ALTER TABLE `medi_user_appoinment` 
ADD COLUMN `Gc_event_id` VARCHAR(45) NULL COMMENT '' AFTER `Active`,
ADD COLUMN `event_type` tinyint(1) COMMENT '0-medi, 1-gc, 3-widget' default 1;


# Add phone code and Emergency code 
ALTER TABLE `medi_doctor` ADD `Code` VARCHAR(5) NULL AFTER `image`, ADD `Emergency_Code` VARCHAR(5) NULL AFTER `Code`;


# Change price format to Varchar - 29-02-2016

ALTER TABLE `medi_clinic_procedure` CHANGE `Price` `Price` VARCHAR(10) NULL DEFAULT NULL;