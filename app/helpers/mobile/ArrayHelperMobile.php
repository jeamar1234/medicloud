<?php

class ArrayHelperMobile{
    
    public static function ClinicProfile($clinicData,$currentDate){
        //StringHelper::Set_Default_Timezone();
        // dd($clinicData);
        if(!empty($clinicData)){
            $clinicOpenTime = self::ProcessClinicOpeningTimes($clinicData->ClinicOPenTime);
            //$clinicActiveHoliday = self::ProcessClinicHolidays($clinicData->ClinicHolidays);
            $clinicActiveHoliday = self::ProcessClinicHolidays($clinicData->ClinicHolidays,$currentDate);
            $clinicOpenStatus = StringHelperMobile::FindClinicOpenStatus(3,$clinicData->ClinicID,$clinicActiveHoliday,$currentDate);
            
            ($clinicData->Email) ? $email =$clinicData->Email : $email = null;
            ($clinicData->Description) ? $descr = $clinicData->Description : $descr = null;
            ($clinicData->Website) ? $website = $clinicData->Website : $website = null;
            ($clinicData->Custom_title) ? $custitle = $clinicData->Custom_title : $custitle = null;
            ($clinicData->Clinic_Price) ? $clprice = $clinicData->Clinic_Price : $clprice = null;
            
            $jsonArray['clinic_id']= $clinicData->ClinicID;
            $jsonArray['name']= $clinicData->CLName;
            $jsonArray['email']= $email;
            $jsonArray['address']= $clinicData->CLAddress.' '.$clinicData->CLCity.' '.$clinicData->CLState.' '.$clinicData->CLPostal;
            $jsonArray['image_url']= $clinicData->CLImage;
            $jsonArray['lattitude']= $clinicData->CLLat;
            $jsonArray['longitude']= $clinicData->CLLng;
            $jsonArray['telephone']= $clinicData->Phone;   
            $jsonArray['description']= $descr;
            $jsonArray['website']= $website;
            $jsonArray['custom_title']= $custitle;
            $jsonArray['clinic_price']= $clprice;
            $jsonArray['open']= $clinicOpenTime;
            $jsonArray['holidays']= $clinicActiveHoliday;
            $jsonArray['open_status']= $clinicOpenStatus;
            //$jsonArray['annotation_url']= $clprice;
            
            return $jsonArray;
        }else{
            return null;
        }
        
    }
    
    public static function ClinicDoctors($clinicDoctors){
        if(!empty($clinicDoctors)){
            foreach($clinicDoctors as $clDoctor){
                $doctorArray = self::ClinicDoctorDetails($clDoctor);
                
                //$doctorArray['doctor_id']= $clDoctor->DoctorID;
                //$doctorArray['name']= $clDoctor->DocName;
                //$doctorArray['qualifications']= $clDoctor->Qualifications;
                //$doctorArray['specialty']= $clDoctor->Specialty;
                //$doctorArray['image_url']= $clDoctor->DocImage;

                $returnArray[] = $doctorArray; 
            }
            $jsonArray = $returnArray;
            return $jsonArray;
        }else{
            $jsonArray = null;
            return $jsonArray;
        }  
    }
    public static function ClinicDoctorDetails($doctordetails){
        if(!empty($doctordetails)){
                $doctorArray['doctor_id']= $doctordetails->DoctorID;
                $doctorArray['name']= $doctordetails->DocName;
                $doctorArray['qualifications']= $doctordetails->Qualifications;
                $doctorArray['specialty']= $doctordetails->Specialty;
                if(empty($doctordetails->DocImage)){
                    $doctorArray['image_url']= URL::to('/assets/images/no-doctor.png');
                }else{
                    $doctorArray['image_url']= $doctordetails->DocImage;
                }
                
            return $doctorArray;
        }else{
            $doctorArray = null;
            return $doctorArray;
        }
    }
    public static function ClinicProcedures($clinicprocedures){
        if(!empty($clinicprocedures)){
            foreach($clinicprocedures as $clProcedure){
                $dataArray = self::ClinicProcedureDetails($clProcedure);
                //$dataArray['procedureid'] = $clProcedure->ProcedureID;
                //$dataArray['name'] = $clProcedure->Name;
                //$dataArray['duration'] = $clProcedure->Duration.' '.$clProcedure->Duration_Format;
                //$dataArray['price'] = $clProcedure->Price;
                $procedureArray[] = $dataArray;
            }
            return $procedureArray;
        }else{
            $procedureArray = null;
            return $procedureArray;
        }
    }
    public static function ClinicProcedureDetails($clinicprocedures){
        if(!empty($clinicprocedures)){
                $dataArray['procedureid'] = $clinicprocedures->ProcedureID;
                $dataArray['name'] = $clinicprocedures->Name;
                $dataArray['duration'] = $clinicprocedures->Duration.' '.$clinicprocedures->Duration_Format;
                $dataArray['price'] = $clinicprocedures->Price;
            return $dataArray;
        }else{
            $dataArray = null;
            return $dataArray;
        }
    }
    
    
    /* Use      :   Used to find Clinic opening times
     * Access   :   Public
     */
    public static function ProcessClinicOpeningTimes($clinicopentimes){
        if($clinicopentimes!=false){
            foreach($clinicopentimes as $CLTimeValue){
                $weeks = StringHelper::GetOpenWeeks($CLTimeValue);
                $cltime['timeid'] = $CLTimeValue->ClinicTimeID;
                $cltime['weeks'] =  $weeks;
                $cltime['starttime'] = $CLTimeValue->StartTime; 
                $cltime['endtime'] =  $CLTimeValue->EndTime; 
                $clinictime[] = $cltime;
            }
            $clinicOpenTime = $clinictime;
        }else{
            $clinicOpenTime = null;
        }
        return $clinicOpenTime;
    }
    
    /* Use      :   Used to find clinic upcoming holidays
     * Access   :   Public 
     */
    public static function ProcessClinicHolidays1($clinicholidays,$currentdate){
        if($clinicholidays!=false){
            $clinicholiday = array();
// dd($clinicholidays);
                foreach($clinicholidays as $CLHolidayValue){
                    if(strtotime($currentdate) <= strtotime($CLHolidayValue->Holiday)){
                        $clholiday['holidayid'] = $CLHolidayValue->ManageHolidayID;
                        $clholiday['type'] = $CLHolidayValue->Type;
                        $clholiday['holiday'] = $CLHolidayValue->Holiday;
                        $clholiday['starttime'] = $CLHolidayValue->From_Time;
                        $clholiday['endtime'] = $CLHolidayValue->To_Time;

                        $clinicholiday[] = $clholiday;
                    }
                }
                $clinicActiveHoliday = $clinicholiday;
             

        }else{
            $clinicActiveHoliday = null;
        }
        return $clinicActiveHoliday;
    }

        public static function ProcessClinicHolidays($clinicholidays,$currentdate){
            if($clinicholidays!=false){
                $clinicholiday = array();
                
                if(is_array($clinicholidays)){
                    foreach($clinicholidays as $CLHolidayValue){
                        if(strtotime($currentdate) <= strtotime($CLHolidayValue->Holiday)){
                            $clholiday['holidayid'] = $CLHolidayValue->ManageHolidayID;
                            $clholiday['type'] = $CLHolidayValue->Type;
                            $clholiday['holiday'] = $CLHolidayValue->Holiday;
                            $clholiday['starttime'] = $CLHolidayValue->From_Time;
                            $clholiday['endtime'] = $CLHolidayValue->To_Time;

                            $clinicholiday[] = $clholiday;
                        }
                    }
                    $clinicActiveHoliday = $clinicholiday;
                } else {

                        if(strtotime($currentdate) <= strtotime($clinicholidays->Holiday)){
                            $clholiday['holidayid'] = $clinicholidays->ManageHolidayID;
                            $clholiday['type'] = $clinicholidays->Type;
                            $clholiday['holiday'] = $clinicholidays->Holiday;
                            $clholiday['starttime'] = $clinicholidays->From_Time;
                            $clholiday['endtime'] = $clinicholidays->To_Time;

                            $clinicholiday[] = $clholiday;
                        }
                    
                    $clinicActiveHoliday = $clinicholiday;

                }

            }else{
                $clinicActiveHoliday = null;
            }
            return $clinicActiveHoliday;
        }
    
    public static function DoctorDetailArray($doctorid,$findWeek,$currentDate){
        if(!empty($doctorid)){
            $findDoctorAvailability = General_Library::FindCurrentDayAvailableTimes(2,$doctorid,$findWeek,strtotime($currentDate));
            $doctorHoliday = General_Library::FindPartyFullDayHolidays(2,$doctorid,$currentDate); 
            (!$doctorHoliday && $findDoctorAvailability) ? $activeDoctorTime = 1 : $activeDoctorTime = 0;
            $doctorDayHolidays = General_Library::FindCurrentDayHolidays(2,$doctorid,$currentDate); 
            
            $doctorDetail['available'] = $activeDoctorTime;
            $doctorDetail['available_times'] = Array_Helper::ClinicAvailabilityArray($findDoctorAvailability);
            $doctorDetail['holidays'] = self::ReturnHolidayArray($doctorDayHolidays);
            $doctorDetail['existingappointments'] = General_Library::FindAllExistingAppointments($doctorid, strtotime($currentDate));
            //$doctorlist[] = $doctorDetail;
        }else{
            $doctorDetail = null;
        }
        return $doctorDetail;
        
        /*
        if(!empty($findAllDoctors)){
            foreach($findAllDoctors as $findDoctor){     
                $doctorDetail = self::DoctorDetails($findDoctor);
                
                $findDoctorAvailability = General_Library::FindCurrentDayAvailableTimes(2,$findDoctor->DoctorID,$findWeek,strtotime($currentDate));
                $doctorHoliday = General_Library::FindPartyFullDayHolidays(2,$findDoctor->DoctorID,$currentDate); 
                (!$doctorHoliday && $findDoctorAvailability) ? $activeDoctorTime = 1 : $activeDoctorTime = 0;
                $doctorDayHolidays = General_Library::FindCurrentDayHolidays(2,$findDoctor->DoctorID,$currentDate);        

                $doctorDetail['available'] = $activeDoctorTime;
                $doctorDetail['available_times'] = self::ClinicAvailabilityArray($findDoctorAvailability);
                $doctorDetail['holidays'] = self::ReturnHolidayArray($doctorDayHolidays);
                $doctorDetail['existingappointments'] = General_Library::FindAllExistingAppointments($findDoctor->DoctorID, strtotime($currentDate));
                $doctorlist[] = $doctorDetail;
            }
        }else{
            $doctorlist = null;
        }
        return $doctorlist; */
    }

    public static function ReturnHolidayArray($findHolidays){
        if($findHolidays){
            // foreach($findHolidays as $holidays){ 
                $clinicholidaydata['holidayid'] = $findHolidays->ManageHolidayID;
                $clinicholidaydata['type'] = $findHolidays->Type;
                $clinicholidaydata['holiday'] = $findHolidays->Holiday;
                $clinicholidaydata['starttime'] = $findHolidays->From_Time;
                $clinicholidaydata['endtime'] = $findHolidays->To_Time;
                $myclinicholiday[] = $clinicholidaydata;
            // }
        }else{
            $myclinicholiday = null;
        }
        return $myclinicholiday;
    }

    //end of Class
}
