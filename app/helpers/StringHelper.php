<?php
class StringHelper{
	public static function encode($password)
	{
		return md5($password);
	}
        public static function getEncryptValue(){
            $encryptValue = sha1(mt_rand(100000,999999).time());
            return $encryptValue;
        }

        public static function requestHeader()
	{
            $getRequestHeader = getallheaders();
            return $getRequestHeader;
	}
        public static function errorMessage($type)
	{
            if($type == "Login"){
                return $message = "Invalid Login";
            }elseif($type=="Register"){
                return $message = "Registration Failed";
            }elseif($type=="Forgot"){
                return $message = "User not found";
            }elseif($type=="EmailExist"){
                return $message = "User email not found";
            }elseif($type=="EmailEmpty"){
                return $message = "Empty email is not allowed";
            }elseif($type=="EmptyValues"){
                return $message = "Cannot accept null values";
            }elseif($type=="NoRecords"){
                return $message = "No records found";
            }elseif($type=="logout"){
                return $message = "Successfully logged out";
            }elseif($type=="logerror"){
                return $message = "There was a problem while log out";
            }elseif($type=="Token"){
                return $message = "Your token is expired";
            }elseif($type=="Update"){
                return $message = "There was a problem while update";
            }elseif($type=="Tryagain"){
                return $message = "There was a problem! Please try again";
            }elseif($type=="QueueBlock"){
                return $message = "Queue Stopped";
            }elseif($type=="Deleted"){
                return $message = "Appointment is Deleted";
            }elseif($type=="NoSlot"){
                return $message = "There is no available slot";
            }elseif($type=="NoQueue"){
                return $message = "This Queue is already allocated";
            }elseif($type=="MoreQueue"){
                return $message = "This Queue number is exceeded";
            }elseif($type=="EmailDuplicate"){
                return $message = "This email already exist";
            }elseif($type=="BlockProfile"){
                return $message = "Your profile has been blocked";
            }elseif($type=="ActiveBooking"){
                return $message = "Sorry we can't put you through this time, there is a scheduled appointment in your list";
            }elseif($type=="OTPError"){
                return $message = "Sorry there is a problem for sending OTP challenge";
            }elseif($type=="PromoError"){
                return $message = "Sorry there is no active promotion on your code";
            }elseif($type=="NoDoctor"){
                return $message = "No Doctors found on this procedure";
            }elseif($type=="NoProcedure"){
                return $message = "No Procedures found under this Doctor";
            }elseif($type=="OpenBooking"){
                return $message = "We are sorry, but you are unable to make multiple appointments";
            }
            
            
            
	}
        
        public static function getDoctorSlotDetails($slotid,$valDate){
            //echo $valDate;
            return function($item) use ($slotid,$valDate) {
            //$nwval = $item['SlotID'] == $slotid;
            $nwval = ($item['SlotID'] == $slotid && $item['Date'] == $valDate);
            return $nwval;
            };
        }
    
        public static function getMySlotValues($slotdetails,$val,$active,$valDate){
            if(is_array($slotdetails) && count($slotdetails)>0){
                $output = array_filter($slotdetails, StringHelper::getDoctorSlotDetails($val,$valDate));
                if(is_array($output) && count($output) >0){
                    $newval = array_values($output);
                    if($newval[0]['SlotDetailID'] =="" || $newval[0]['SlotDetailID']==null){
                        return null;
                    }else{
                        if($active==1){
                            return $newval[0]['Active'];
                        }else{
                            
                            return $newval[0]['SlotDetailID'];
                        }        
                    }
                }else{
                    return null;
                }
            }
        }
        
        /* Use      :   Used to find age from date of birth
         * 
         * 
         */
        public static function findAge($Date){
            $date = new DateTime($Date);
            $now = new DateTime();
            $interval = $now->diff($date);
            return $interval->y;
        }
        
        /* Use          :   Used to manage Medicloud session
         * User group   :   User, Clinic and Doctor
         * 
         */
        public static function getAuthSession(){
            $value = Session::get('user-session');
            //$findUser = App_AuthController::getUserDetails($value);
            $findUser = Auth_Library::ValidateLoginSession($value);
            return $findUser;
        }
        
        /* Use          :   Used to manage Medicloud session
         * User group   :   User, Clinic and Doctor
         * 
         */
        public static function getMainSession($getGroup){
            $value = Session::get('user-session');
            //$findUser = App_AuthController::getUserDetails($value);
            $findUser = Auth_Library::ValidateLoginSession($value);
            
            if($findUser){              
                return $findUser;
            
            /*if(count($findUser) >0){
                if($getGroup == 2){
                    return StringHelper::doctorSession($findUser);
                }elseif($getGroup == 3){
                    return StringHelper::clinicSession($findUser);
                } */
            }else{
                Session::forget('user-session');
                return FALSE;
            }       
        }
        
        //Private function only accessible by this class
        private function userSession(){
            // need to implement later for user
        }
        //Private function only accessible by this class
        private static function doctorSession($findUser){
            if(count($findUser)> 0 && $findUser->UserType == 2 && ($findUser->Ref_ID != null || $findUser->Ref_ID != "")){
                return $findUser;
            }else{
                Session::forget('user-session');
                return FALSE;
            }
        }
        //Private function only accessible by this class
        private static function clinicSession($findUser){
            if(count($findUser)> 0 && $findUser->UserType == 3 && ($findUser->Ref_ID != null || $findUser->Ref_ID != "")){
                return $findUser;
            }else{
                Session::forget('user-session');
                return FALSE;
            }
        }
        /* Use          :   Used to find current time
         * 
         */
        public static function CurrentTime(){
            //$dateTime = new DateTime('now', new DateTimeZone('Asia/Singapore'));
            $dateTime = self::TimeZone();
            $timezone = $dateTime->format('H.i');
            return $timezone;
        }
        /* Use          :   Use to find current time with AM/PM
         * 
         */
        public static function CurrentTimeSetup($currentdate){
            //$dateTime = new DateTime('now', new DateTimeZone('Asia/Singapore'));
            $dateTime = self::TimeZone();
            if(ArrayHelper::ActivePlusDate($currentdate)==1){
                $timezone = 0;
            }else{
                $timezone = $dateTime->format('H.iA');
            }
            return $timezone;
        }
        
        /* Use          :   Used to process time zone
         * 
         */
        public static function TimeZone(){
            $dateTime = new DateTime('now', new DateTimeZone('Asia/Singapore'));
            return $dateTime;
        }
        
        
        public static function OTPChallenge(){
            $otpChallenge = mt_rand(100000, 999999);
            return $otpChallenge;
        }
        
        public static function SendOTPSMS($mobileno,$message){
            $fields = json_encode(array('from' => 'Medicloud',
                'to' => intval($mobileno),
                'message' => $message
                ));
            $url = 'https://rest.tyntec.com/sms/v1/outbound/requests';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, "medicloud:s2fm2Aqr");

            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if($info['http_code']==200){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        public static function TestSendOTPSMS($mobileno,$message){
            $fields = json_encode(array('from' => 'Medicloud',
                'to' => intval($mobileno),
                'message' => 'Your One Time Passcode for Medicloud - '.$message
                ));
            $url = 'https://rest.tyntec.com/sms/v1/outbound/requests';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, "medicloud:s2fm2Aqr");

            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if($info['http_code']==200){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        
        
        public static function GetWeekFromTime(){
            self::Set_Default_Timezone();
            $currentWeek = date("w", time());
            if($currentWeek==0){
                $nowWeek = "Sun";
            }elseif($currentWeek==1){
                $nowWeek = "Mon";
            }elseif($currentWeek==2){
                $nowWeek = "Tue";
            }elseif($currentWeek==3){
                $nowWeek = "Wed";
            }elseif($currentWeek==4){
                $nowWeek = "Thu";
            }elseif($currentWeek==5){
                $nowWeek = "Fri";
            }elseif($currentWeek==6){
                $nowWeek = "Sat";
            }else{
                $nowWeek = null;
            }
            return $nowWeek;
        }
        
        public static function Set_Default_Timezone(){
            date_default_timezone_set("Asia/Singapore"); 
        }
        
        
        public static function GetClinicOpenTimes($clinicid){
            $findClinicTimes = ClinicLibrary::FindClinicTimes($clinicid);         
            if($findClinicTimes){
                //$weeks = array();
                foreach($findClinicTimes as $ctvalue){ 
                    $weeks = self::GetOpenWeeks($ctvalue);
                    $cltime['timeid'] = $ctvalue->ClinicTimeID;
                    $cltime['weeks'] =  $weeks;
                    $cltime['starttime'] = $ctvalue->StartTime; 
                    $cltime['endtime'] =  $ctvalue->EndTime; 
                    $cltimeval[] = $cltime;
                }
            }else{
                $cltimeval = null;
            }
            return $cltimeval;
        }
        
        public static function GetOpenWeeks($ctvalue){
            $weeks = array();
            if($ctvalue->Mon==1){
                $weeks[] = "Mon";
            }if($ctvalue->Tue==1){
                $weeks[] = "Tue";
            }if($ctvalue->Wed==1){
                $weeks[] = "Wed";
            }if($ctvalue->Thu==1){
                $weeks[] = "Thu";
            }if($ctvalue->Fri==1){
                $weeks[] = "Fri";
            }if($ctvalue->Sat==1){
                $weeks[] = "Sat";
            }if($ctvalue->Sun==1){
                $weeks[] = "Sun";
            }
            $finalValue = implode(', ', $weeks);
            return $finalValue;
        }
        
        public static function GetClinicOpenStatus($clinicid){
            $findWeek = self::GetWeekFromTime();
            $findClinicTimeStatus = ClinicLibrary::FindClinicTimesStatus($clinicid,$findWeek);
            $openstatus = 0;
            if($findClinicTimeStatus){
                $timecounter = 0;
                foreach($findClinicTimeStatus as $ctstatus){
                    if($timecounter ==0){                       
                        if(strtotime($ctstatus->StartTime) <= time() && strtotime($ctstatus->EndTime) >=time()){
                            $openstatus = 1;
                            $timecounter = 1;
                        }
                    }
                }
            }
            return $openstatus;
        }
        
        /* Use      :   Used to find stopping repeat time
         * 
         */
        public static function FindStopRepeatDate($currentDate){
            self::Set_Default_Timezone();
            $date_string = $currentDate;
            $date = new DateTime($date_string);

            $weekno = date("w", strtotime($date_string));

            $currentValue = 7 - $weekno;
            if($currentValue != 7){
                $newDate = $date->modify("+".$currentValue." day"); 
            }else{
                $newDate = date('d-m-Y');
            }
            $finalDate = $date->format("d-m-Y");

            return $finalDate;
        }
        
        public static function FindWeekFromDate($currentDate){
            self::Set_Default_Timezone();
            $weekno = date("w", strtotime($currentDate));
            $findWeek = '';
            if($weekno==0){
                $findWeek = "Sun";
            }elseif($weekno==1){
                $findWeek = "Mon";
            }if($weekno==2){
                $findWeek = "Tue";
            }if($weekno==3){
                $findWeek = "Wed";
            }if($weekno==4){
                $findWeek = "Thu";
            }if($weekno==5){
                $findWeek = "Fri";
            }if($weekno==6){
                $findWeek = "Sat";
            }
            return $findWeek;
        }
        
        public static function Deployment(){
            $config = Config::get('config.deployment'); 
            if($config=="Production"){
                return 1;
            }else{
                return 0;
            }
        }
}