<?php

class EmailHelper{
    //$eh = new EmailHelper();
    public static function sendEmail($dataArray){
        
        Mail::send($dataArray['emailPage'], $dataArray, function($message) use ($dataArray){       
            //$message->from('riz_ris@yahoo.com', 'MediCloud');
            $message->from('noreply@medicloud.sg', 'MediCloud');
            //$message->attach($pathToFile);
            $message->to($dataArray['emailTo'],$dataArray['emailName']);
            $message->subject($dataArray['emailSubject']);
            
        }); 
    }
    public static function QueueEmail($dataArray){
        Mail::queue($dataArray['emailPage'], $dataArray, function($message) use ($dataArray){
            $message->from('noreply@medicloud.sg', 'MediCloud');
            $message->to($dataArray['emailTo'],$dataArray['emailName']);
            $message->subject($dataArray['emailSubject']);
        });
    }
     //Mail::queue('emails.activate', $data, function($message) use ($user){
     //     $message->from('no-reply@mysite.com', 'Mysite.com');
    ///      $message->to($user->email, $user->username)->subject('Welcome');
    //    });
    
    
}