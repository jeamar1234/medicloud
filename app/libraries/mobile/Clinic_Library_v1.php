<?php

class Clinic_Library_v1{
    public static function FindClinic($clinicid){
        $clinic = new Clinic();
        $clinicData = $clinic->ClinicDetails($clinicid);
        if($clinicData){
            return $clinicData;
        }else{
            return FALSE;
        }
    }
    
    public static function FindClinicProfile($clinicid){
        $clinic = new Clinic();
        $clinicData = $clinic->FindClinicProfile($clinicid);
        if($clinicData){
            return $clinicData;
        }else{
            return FALSE;
        }
    }
    
    /* Use      :   Used to send clinic details service 
     * Access   :   Public Mobile
     */
    public static function ClinicDetails($clinicid){
        StringHelper::Set_Default_Timezone();
        $returnObject = new stdClass();
        $currentTime = date('d-m-Y');
        $clinicData = self::FindClinicProfile($clinicid);
        if($clinicData){
            
            
            $clinicProcedures = self::FindClinicProcedures($clinicid);
            $findClinicTimes = General_Library::FindAllClinicTimes(3,$clinicData->ClinicID,  strtotime($currentTime));
            $findClinicHolidays = General_Library::FindUpcomingHolidays(3,$clinicData->ClinicID,$currentTime);
            $clinicData->ClinicOPenTime = $findClinicTimes;
            $clinicData->ClinicHolidays = $findClinicHolidays;
            $clinicData->Email = '----';
            $jsonArray = ArrayHelperMobile::ClinicProfile($clinicData,$currentTime);
            
            $doctorsForClinic = Doctor_Library_v1::FindAllClinicDoctors($clinicData->ClinicID); 
            if($doctorsForClinic){
                $jsonArray['doctor_count'] = count($doctorsForClinic);
                $jsonArray['doctors'] = ArrayHelperMobile::ClinicDoctors($doctorsForClinic);   
            }else{
                $jsonArray['doctor_count'] = 0;
                $jsonArray['doctors'] = null;
            }
            $jsonArray['clinic_procedures'] = ArrayHelperMobile::ClinicProcedures($clinicProcedures);
            
            $returnObject->status = TRUE;
            $returnObject->data = $jsonArray;   
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("NoRecords");  
        }
        return $returnObject;  
    }
    
    /* Use     :   Used to get all clinic procedures 
     * Access   :   Public 
     */
    public static function FindClinicProcedures($clinicid){
        $clinicprocedure = new ClinicProcedures();
        $findClinicProcedure = $clinicprocedure->FindClinicProcedures($clinicid);
        if($findClinicProcedure){
            return $findClinicProcedure;
        }else{
            return FALSE;
        }
    }
    
    /* Use      :   Used to get doctors from a particular procedure
     * Access   :   PUBLIC
     * 
     */
    public static function ProcedureDetails($procedureid){
        $returnObject = new stdClass();
        $findClinicProcedure = self::FindClinicProcedure($procedureid);
        if($findClinicProcedure){
            $findDoctorProcedure = Doctor_Library_v1::FindDoctorsByProcedure($procedureid,$findClinicProcedure->ClinicID);
            if($findDoctorProcedure){
                $doctorArray = ArrayHelperMobile::ClinicDoctors($findDoctorProcedure); 
                $dataArray = ArrayHelperMobile::ClinicProcedureDetails($findClinicProcedure);

                $dataArray['doctors'] = $doctorArray;
                
                $returnObject->status = TRUE;
                $returnObject->data = $dataArray;
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("NoDoctor");  
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("NoRecords"); 
        }
        return $returnObject;  
    }
    
    /* Use          :   Used to find doctors procedure list 
     * Access       :   Public 
     * Parameter    :   doctor id 
     */
    public static function ClinicDoctorProcedures($doctorid){
        $returnObject = new stdClass();
        $findDoctor = Doctor_Library_v1::FindDoctorDetails($doctorid);
        if($findDoctor){
            $findDoctor->DocName = $findDoctor->Name;
            $findDoctor->DocImage = $findDoctor->image;
            
            $dataArray = ArrayHelperMobile::ClinicDoctorDetails($findDoctor);
            //$dataArray['doctorid'] = $findDoctor->DoctorID;
            $doctorProcedureList = Doctor_Library_v1::FindDoctorsBProcedureList($findDoctor->DoctorID);
            if($doctorProcedureList){
                $findProcedureList = ArrayHelperMobile::ClinicProcedures($doctorProcedureList);
                $dataArray['clinic_procedures'] = $findProcedureList;
                
                $returnObject->status = TRUE;
                $returnObject->data = $dataArray;
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("NoProcedure");
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("NoRecords");
        }
        return $returnObject;
    }
    
    

    /* Use          :   Used to find clinic procedure details
     * Parameter    :   procedure id 
     */ 
    public static function FindClinicProcedure($procedureid){
        if(!empty($procedureid)){
            $clinicprocedure = new ClinicProcedures();
            $findClinicPorcedure = $clinicprocedure->ClinicProcedureByID($procedureid);
            if($findClinicPorcedure){
                return $findClinicPorcedure;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }  
    }
    
    public static function AppointmentHistory($findUserID){
        $currentdate = date('d-m-Y');
        $returnObject = new stdClass();
        //$allAppointments = ClinicLibrary::FindAllUserAppointment($findUserID);
        $allAppointments = General_Library_Mobile::FindUserAppointments($findUserID);
        if($allAppointments){
            $upcoming = null; $history = null;
            
            foreach($allAppointments as $appointment){
                $findPatientCount = General_Library_Mobile::NumberOfBookings($appointment->DoctorID,$appointment->BookDate);
                //$findClinicProcedure = General_Library::FindClinicProcedure($appointment->ProcedureID);
                $findClinicProcedure = General_Library::ClinicProcedureBoth($appointment->ProcedureID);
                //$findClinicDoctor = Doctor_Library_v1::FindSingleClinicDoctor($findClinicProcedure->ClinicID,$appointment->DoctorID);
                $findClinicDoctor = Doctor_Library_v1::FindSingleClinicDoctorBoth($findClinicProcedure->ClinicID,$appointment->DoctorID);
                $findClinicTimes = General_Library::FindAllClinicTimes(3,$findClinicProcedure->ClinicID, strtotime($currentdate));
                $clinicOpenTime = ArrayHelperMobile::ProcessClinicOpeningTimes($findClinicTimes);
                
                $dataBook['booking']['booking_id'] = $appointment->UserAppoinmentID;
                $dataBook['booking']['type'] = 2;
                $dataBook['booking']['date'] = date('d-m-Y',$appointment->BookDate);
                $dataBook['booking']['time'] = date('h:i A',$appointment->StartTime).' - '.date('h:i A',$appointment->EndTime);
                $dataBook['booking']['no_of_patients'] = $findPatientCount;
                //$dataBook['booking']['now_serving'] = $nowSaving;
                $dataBook['booking']['fee'] = $findClinicProcedure->Price;
                $dataBook['booking']['queue_no'] = $appointment->BookNumber;
                
                $dataBook['doctor']['doctor_id'] = $findClinicDoctor->DoctorID;
                $dataBook['doctor']['name'] = $findClinicDoctor->DocName;
                $dataBook['doctor']['qualifications'] = $findClinicDoctor->Qualifications;
                $dataBook['doctor']['specialty'] = $findClinicDoctor->Specialty;
                $dataBook['doctor']['image_url'] = $findClinicDoctor->DocImage;
                
                $dataBook['clinic']['clinic_id'] = $findClinicDoctor->ClinicID;
                //$dataBook['clinic']['annotation_url'] = $findInsurance['annotation_url'];       
                $dataBook['clinic']['image_url'] = $findClinicDoctor->CliImage;
                $dataBook['clinic']['name'] = $findClinicDoctor->CliName;
                $dataBook['clinic']['address'] = $findClinicDoctor->Address;
                $dataBook['clinic']['lattitude'] = $findClinicDoctor->Lat;
                $dataBook['clinic']['longitude'] = $findClinicDoctor->Lng;
                $dataBook['clinic']['telephone'] = $findClinicDoctor->CliPhone;
                $dataBook['clinic']['open'] = $clinicOpenTime;
                $dataBook['clinic']['doctor_count'] = DoctorLibrary::FindDoctorCount($findClinicDoctor->ClinicID);
                //$dataBook['clinic']['panel_insurance']['insurance_id'] = $findInsurance['insurance_id'];
                //$dataBook['clinic']['panel_insurance']['name'] = $findInsurance['name'];
                //$dataBook['clinic']['panel_insurance']['image_url'] = URL::to('/assets/'.$findInsurance['image_url']);   
                if($appointment->Status == 0){
                    $upcoming[] = $dataBook;
                }else{
                    $history[] = $dataBook;
                }
                
            } 
            $totalEvents['upcoming'] = $upcoming;
            $totalEvents['history'] = $history;
            $returnObject->status = TRUE;
            $returnObject->data = $totalEvents;
            
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("NoRecords");
        }
        return $returnObject;
    }

    
    public static function AppointmentDetails($findUserID,$appointmentid){
        $returnObject = new stdClass();
        $appointment = ClinicLibrary::FindAppointment($appointmentid);
        if($appointment){
                $findPatientCount = General_Library_Mobile::NumberOfBookings($appointment->DoctorID,$appointment->BookDate);
                //$findClinicProcedure = General_Library::FindClinicProcedure($appointment->ProcedureID);
                $findClinicProcedure = General_Library::ClinicProcedureBoth($appointment->ProcedureID);
                //$findClinicDoctor = Doctor_Library_v1::FindSingleClinicDoctor($findClinicProcedure->ClinicID,$appointment->DoctorID);
                $findClinicDoctor = Doctor_Library_v1::FindSingleClinicDoctorBoth($findClinicProcedure->ClinicID,$appointment->DoctorID);
                $findClinicTimes = General_Library::FindAllClinicTimes(3,$findClinicProcedure->ClinicID, $appointment->BookDate);
                $clinicOpenTime = ArrayHelperMobile::ProcessClinicOpeningTimes($findClinicTimes);
                
                $dataBook['booking']['booking_id'] = $appointment->UserAppoinmentID;
                $dataBook['booking']['type'] = 2;
                $dataBook['booking']['date'] = date('d-m-Y',$appointment->BookDate);
                //$dataBook['booking']['time'] = date('h:i A',$appointment->StartTime).' - '.date('h:i A',$appointment->EndTime);
                $dataBook['booking']['time'] = date('h:i A',$appointment->StartTime);
                $dataBook['booking']['no_of_patients'] = $findPatientCount;
                //$dataBook['booking']['now_serving'] = $nowSaving;
                $dataBook['booking']['fee'] = $findClinicProcedure->Price;
                $dataBook['booking']['queue_no'] = $appointment->BookNumber;
                
                $dataBook['procedure']['procedure_id'] = $findClinicProcedure->ProcedureID;
                $dataBook['procedure']['name'] = $findClinicProcedure->Name;
                $dataBook['procedure']['duration'] = $findClinicProcedure->Duration;
                $dataBook['procedure']['price'] = $findClinicProcedure->Price;
                
                $dataBook['doctor']['doctor_id'] = $findClinicDoctor->DoctorID;
                $dataBook['doctor']['name'] = $findClinicDoctor->DocName;
                $dataBook['doctor']['qualifications'] = $findClinicDoctor->Qualifications;
                $dataBook['doctor']['specialty'] = $findClinicDoctor->Specialty;
                $dataBook['doctor']['image_url'] = $findClinicDoctor->DocImage;
                
                $dataBook['clinic']['clinic_id'] = $findClinicDoctor->ClinicID;
                //$dataBook['clinic']['annotation_url'] = $findInsurance['annotation_url'];       
                $dataBook['clinic']['image_url'] = $findClinicDoctor->CliImage;
                $dataBook['clinic']['name'] = $findClinicDoctor->CliName;
                $dataBook['clinic']['address'] = $findClinicDoctor->Address;
                $dataBook['clinic']['lattitude'] = $findClinicDoctor->Lat;
                $dataBook['clinic']['longitude'] = $findClinicDoctor->Lng;
                $dataBook['clinic']['telephone'] = $findClinicDoctor->CliPhone;
                $dataBook['clinic']['open'] = $clinicOpenTime;
                $dataBook['clinic']['doctor_count'] = DoctorLibrary::FindDoctorCount($findClinicDoctor->ClinicID);
           
            $returnObject->status = TRUE;
            $returnObject->data = $dataBook;       
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("NoRecords");
        }
        return $returnObject;
    }
    
    public static function ProcessSearch($UserID){
        $currentTime = date('d-m-Y');
        $search = Input::get('search');
        $returnObject = new stdClass();
        if(!empty($search)){
            $clinicData = ClinicLibrary::FindClinicSearch($search);
            if($clinicData){
                //$panelCount = null;
                foreach($clinicData as $cdata){ 
                    $userprofile = AuthLibrary::FindUserProfileByRefID($cdata->ClinicID);
                    if(!empty($userprofile->Email)){
                        $clinicemail = $userprofile->Email;
                    }else{
                        $clinicemail = null;
                    }
                    $clinicProcedures = self::FindClinicProcedures($cdata->ClinicID);
                    $findClinicTimes = General_Library::FindAllClinicTimes(3,$cdata->ClinicID,  strtotime($currentTime));
                    $findClinicHolidays = General_Library::FindUpcomingHolidays(3,$cdata->ClinicID,$currentTime);
                    $cdata->ClinicOPenTime = $findClinicTimes;
                    $cdata->ClinicHolidays = $findClinicHolidays;
                    $cdata->Email = $clinicemail;
                    $jsonArray = ArrayHelperMobile::ClinicProfile($cdata,$currentTime);
                    
                    $jsonArray['doctor_count']= DoctorLibrary::FindDoctorCount($cdata->ClinicID);
                    $jsonArray['clinic_procedures'] = ArrayHelperMobile::ClinicProcedures($clinicProcedures);
                    
                    $returnArray[] = $jsonArray;
                }
                $returnObject->status = TRUE;
                $returnObject->data['total_count'] = count($clinicData);
                $returnObject->data['panel_count'] = 0;
                $returnObject->data['panel_insurance'] =null;
                $returnObject->data['clinics'] = $returnArray;
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("NoRecords");
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("EmptyValues"); 
        }
        return $returnObject;   
    }
    
    
    public static function ProcessNearby($findUserID){
        $currentTime = date('d-m-Y');
        $getLat = Input::get('lat');
        $getLng = Input::get('lng');
        $getType = Input::get('type');
        $returnObject = new stdClass();
        
        if(!empty($getLat) && !empty($getLng)&& !empty($getType)){
            $findNearbyData = ClinicLibrary::FindNearby($getLat,$getLng,$getType);
            if($findNearbyData){
                $clinictype =0; $clinictypename = null; $count=1;
                foreach($findNearbyData as $cdata){
                    $userprofile = AuthLibrary::FindUserProfileByRefID($cdata->ClinicID);
                    if(!empty($userprofile->Email)){
                        $clinicemail = $userprofile->Email;
                    }else{
                        $clinicemail = null;
                    }
                    if($count ==1) {
                        $clinictype = $cdata->ClinicTypeID; $clinictypename = $cdata->ClinicType;
                        $count++;
                    }
                    $clinicProcedures = self::FindClinicProcedures($cdata->ClinicID);
                    $findClinicTimes = General_Library::FindAllClinicTimes(3,$cdata->ClinicID,  strtotime($currentTime));
                    $findClinicHolidays = General_Library::FindUpcomingHolidays(3,$cdata->ClinicID,$currentTime);
                    $cdata->ClinicOPenTime = $findClinicTimes;
                    $cdata->ClinicHolidays = $findClinicHolidays;
                    $cdata->Email = $clinicemail;
                    $jsonArray = ArrayHelperMobile::ClinicProfile($cdata,$currentTime);
                    
                    $jsonArray['doctor_count']= DoctorLibrary::FindDoctorCount($cdata->ClinicID);
                    $jsonArray['clinic_procedures'] = ArrayHelperMobile::ClinicProcedures($clinicProcedures);
                    
                    $jsonArray['doctor_count']= DoctorLibrary::FindDoctorCount($cdata->ClinicID);
                    
                    $findNonePanelInsurance = ClinicLibrary::FindClinicTypeAnnotation($getType);
                    if($findNonePanelInsurance){ 
                        if($jsonArray['open_status'] ==1){
                            $jsonArray['annotation_url']= $findNonePanelInsurance->Annotation;
                        }else{
                            $jsonArray['annotation_url']= $findNonePanelInsurance->Annotation_Default;
                        }
                    }else{
                        $jsonArray['annotation_url']= null;
                    }                  
                    $jsonArray['panel_insurance'] =null;
                    
                    
                    $returnArray[] = $jsonArray;
                }
                $returnObject->status = TRUE;
                
                
                $returnObject->data['user_primary_insurance'] = null;
                $returnObject->data['total_count'] = count($findNearbyData);
                $returnObject->data['panel_count'] = 0;
                $returnObject->data['clinic_type'] = $clinictype;
                $returnObject->data['clinic_type_name'] = $clinictypename;
                $returnObject->data['clinics'] = $returnArray;
                
                
                
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("NoRecords");  
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("EmptyValues");   
        }  
        return $returnObject;
    }
    //End of class
}