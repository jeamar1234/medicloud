<?php
class AuthLibrary{
    
    
    public static function Login(){
        $returnObject = new stdClass();
        $token = Authorizer::issueAccessToken();
        if($token){
            $returnObject->error= "false";
            $returnObject->data = $token;
            $findUserID = self::FindUserFromToken($token['access_token']); 
            $findRealAppointment = ClinicLibrary::FindRealAppointment($findUserID);
            $activePromoCode = General_Library::ActivePromoCode(); 
            if(!$findRealAppointment){
            //if($activePromoCode && !$findRealAppointment){    
                $returnObject->data['promocode']['status'] = 1;
                //$returnObject->data['promocode']['message'] = "Welcome, we are very excited to have you onboard, you are entitled for a $15 Discount on your first booking via Medicloud, please enter the promo code '".$activePromoCode->Code."' upon confirmation of your appointment to claim your discount";
                $returnObject->data['promocode']['message'] = "We are very excited to have you onboard, select the clinic of your choice and start booking for your next appointment now!";
            }else{
                $returnObject->data['promocode'] = null;
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("Login");
        }
        return $returnObject; 
    }

    public static function validToken(){
        $authController = new Api_V1_AuthController();
        $findUserID = $authController->returnValidToken();
        if($findUserID){
            return $findUserID;
        }
    }
    public static function SignUp(){
        $returnObject = new stdClass();
        $inputdata = Input::all();
        if(!empty($inputdata)){
            $dataArray['name']= $inputdata['full_name'];
            $dataArray['password']= StringHelper::encode($inputdata['password']);
            $dataArray['email']= $inputdata['email'];
            $dataArray['mobile']= 0;
            $dataArray['latitude']= $inputdata['latitude'];
            $dataArray['longitude']= $inputdata['longitude'];
            $dataArray['nric']= null;
            $dataArray['fin']= null;
            $dataArray['createdat']=time();
            $dataArray['active']=1;
            $dataArray['usertype']=1;
            
            $userExist = self::EmailExist($inputdata['email']);
            if($userExist){
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("EmailDuplicate");
            }else{
                $userID = self::AddNewUser($dataArray);
                if($userID){
                    $activePromoCode = General_Library::ActivePromoCode();
                    $emailDdata['emailName']= $inputdata['full_name'];
                    $emailDdata['emailPage']= 'email-templates.welcome';
                    $emailDdata['emailTo']= $inputdata['email'];
                    $emailDdata['emailSubject'] = 'Thank you for registering with us';

                    $emailDdata['activeLink'] = "<a href='".URL::to('app/auth/login')."'> Find out more </a>";
                    EmailHelper::sendEmail($emailDdata);

                    $returnObject->status = TRUE;
                    $returnObject->data['userid'] = $userID;
                    //if($activePromoCode){
                        $returnObject->data['promocode']['status'] = 1;
                        //$returnObject->data['promocode']['message'] = "Welcome, we are very excited to have you onboard, you are entitled for a $15 Discount on your first booking via Medicloud, please enter the promo code '".$activePromoCode->Code."' upon confirmation of your appointment to claim your discount";
                        $returnObject->data['promocode']['message'] = "We are very excited to have you onboard, select the clinic of your choice and start booking for your next appointment now!";
                    //}else{
                    //    $returnObject->data['promocode'] = null;
                    //}
                    
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Register");
                }
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("EmptyValues");
        }
        return $returnObject;  
    }
    
    
    public static function SignUp_Old(){
        $user = new User();
        $userpolicy = new UserInsurancePolicy();
        $returnObject = new stdClass();
        $dataArray['name']= Input::get ('full_name');
        $dataArray['password']= StringHelper::encode(Input::get ('password'));
        $dataArray['email']= Input::get ('email');
        $dataArray['mobile']= Input::get('mobile_phone');
        $dataArray['insuranceid']= Input::get ('insurance_company');
        $dataArray['policyname']= Input::get ('insurance_policy_name');
        $dataArray['policyno']= Input::get ('insurance_policy_no');
        $dataArray['isprimary']= 1;
        $dataArray['latitude']= Input::get ('latitude');
        $dataArray['longitude']= Input::get ('longitude');
        $dataArray['nric']= Input::get ('nric');
        $dataArray['fin']= null;
        $dataArray['createdat']=time();
        $dataArray['active']=1;
        $dataArray['usertype']=1;
        
        $findUserEmail = $user->checkEmail(Input::get ('email'));
        if($findUserEmail){
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("EmailDuplicate");
        }else{
            $userID = $user->authSignup($dataArray);
            if($userID){
                $emailDdata['emailName']= Input::get ('full_name');
                $emailDdata['emailPage']= 'email-templates.welcome';
                $emailDdata['emailTo']= Input::get ('email');
                $emailDdata['emailSubject'] = 'Thank you for registering with us';

                $emailDdata['activeLink'] = "<a href='".URL::to('app/auth/login')."'> Find out more </a>";
                EmailHelper::sendEmail($emailDdata);
                    
                $dataArray['userid'] = $userID; 
                $insertUserPolicy = $userpolicy->addInsurancePolicy($dataArray);
                $returnObject->status = TRUE;
                $returnObject->data['userid'] = $userID;
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Register");
            }
        }
        return $returnObject;
    }

    
    public static function ProfileUpdate($userid){
        //$user = new User();
        $insurancepolicy = new UserInsurancePolicy();
        $returnObject = new stdClass();
        //$allUpdatedata = Input::all();
        if(!empty($userid)){        
            if(Input::get('full_name')) {
                $dataArray['Name'] = Input::get('full_name');
            }if(Input::get('email')) {
                $dataArray['Email'] = Input::get('email');
            }if(Input::get('nric')) {
                $dataArray['NRIC'] = Input::get('nric');
            }if(Input::get('mobile_phone')) {
                $dataArray['PhoneNo'] = Input::get('mobile_phone');
            }if(Input::get('dob')) {
                $newDate = date("d-m-Y", strtotime(Input::get('dob')));
                $dataArray['DOB'] = $newDate;
                $dataArray['Age'] = StringHelper::findAge($newDate);
            }if(Input::get('weight')) {
                $dataArray['Weight'] = Input::get('weight');
            }if(Input::get('height')) {
                $dataArray['Height'] = Input::get('height');
            }if(Input::get('bmi')) {
                $dataArray['Bmi'] = Input::get('bmi');
            }if(Input::get('blood_type')) {
                $dataArray['Blood_Type'] = Input::get('blood_type');
            }if(!empty(Input::get('photo_url'))){    
                $dataArray['Image'] = Input::get('photo_url');
            }
            /*if(!empty(Input::file('file'))){
                $uploadFile = Image_Library::ImageResizing(1,100);
                if($uploadFile){
                    $dataArray['Image'] = Input::get($uploadFile);
                }
            } */   
            $dataArray['userid'] = $userid;
            $dataArray['updated_at'] = time();
            //$updateUserProfile = $user->updateUserProfile($dataArray);
            $updateUserProfile = self::UpdateUserProfile($dataArray);
            if($updateUserProfile){                   
                $returnObject->status = TRUE;
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Update");
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("Update");
        }
        return $returnObject;
        
        
        
        /*
        $allUpdatedata = Input::all();
            $user = new User();
            $insurancepolicy = new UserInsurancePolicy();
            $returnObject = new stdClass();

            if(is_array($allUpdatedata) && count($allUpdatedata) >0 ){
                if(Input::get('full_name')) {
                    $dataArray['Name'] = Input::get('full_name');
                }if(Input::get('email')) {
                    $dataArray['Email'] = Input::get('email');
                }if(Input::get('nric')) {
                    $dataArray['NRIC'] = Input::get('nric');
                }if(Input::get('mobile_phone')) {
                    $dataArray['PhoneNo'] = Input::get('mobile_phone');
                }if(Input::get('dob')) {
                    $newDate = date("d-m-Y", strtotime(Input::get('dob')));
                    $dataArray['DOB'] = $newDate;
                    $dataArray['Age'] = StringHelper::findAge($newDate);
                }if(Input::get('weight')) {
                    $dataArray['Weight'] = Input::get('weight');
                }if(Input::get('height')) {
                    $dataArray['Height'] = Input::get('height');
                }if(Input::get('bmi')) {
                    $dataArray['Bmi'] = Input::get('bmi');
                }
                if(Input::get('insurance_id')) {
                    $insurance_exist = 1;
                    //$dataArrayPolicy['InsuaranceCompanyID'] = Input::get('insurance_company');
                    $dataArrayPolicy['InsuaranceCompanyID'] = Input::get('insurance_id');
                }if(Input::get('insurance_policy_no')) {
                    $insurance_exist = 1;
                    $dataArrayPolicy['PolicyNo'] = Input::get('insurance_policy_no');
                }
                $dataArrayPolicy['userid'] = Input::get('user_id');
                $dataArray['userid'] = Input::get('user_id');
                $dataArray['updated_at'] = time();
                        
                $updateUserProfile = $user->updateUserProfile($dataArray);
                if($insurance_exist == 1){
                    $findInsurance = $insurancepolicy->getUserInsurancePolicyi(Input::get('user_id'));
                    if($findInsurance){
                        $policyStatus = $insurancepolicy->updateInsurancePolicy($dataArrayPolicy);
                    }else{
                        $policyStatus = $insurancepolicy->addInsurancePolicy($dataArrayPolicy);
                    }
                }
              
                if($updateUserProfile || $policyStatus){                   
                    $returnObject->status = TRUE;
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Update");
                }
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("EmailEmpty");
            }
            return Response::json($returnObject);
         *
         */
    }
    
    public static function FindUserIDByEmail($email){
        $user = new User();
        $findUserByEmail = $user->Forgot_Password($email);
        if($findUserByEmail){
            return $findUserByEmail;
        }else{
            return FALSE;
        }
    }

    public static function Delete_Token(){
        $AccessToken = new OauthAccessTokens();
        $getRequestHeader = StringHelper::requestHeader();
        //if($getRequestHeader['Authorization'] !=""){
        if(!empty($getRequestHeader['Authorization'])){    
            $getAccessToken = $AccessToken->FindToken($getRequestHeader['Authorization']);
            if($getAccessToken){
                $deleteToken = $AccessToken->DeleteToken($getAccessToken->id);
                if($deleteToken){
                    return TRUE;
                }else{
                    return FALSE;
                }
            }
        }else{
            return FALSE;
        }
    }

    public static function Forgot_Password(){
        $email = Input::get ('email');
        $returnObject = new stdClass();
        if(!empty($email)){
            $findUserID = self::FindUserIDByEmail($email);
            if($findUserID){
                $returnObject->status = TRUE;
                //$returnObject->data['userid'] = $findUserID;
                $returnObject->message = "Password reset instructions have been sent to your email";
                $deleteToken = self::Delete_Token();
                
                $updateArray['userid'] = $findUserID;
                $updateArray['ResetLink'] = StringHelper::getEncryptValue();
                $updateArray['Recon'] = 0;
                $updateArray['updated_at'] = time();
                $userUpdated = self::UpdateUserProfile($updateArray);
                if($userUpdated){
                    $findNewUser = self::FindUserProfile($findUserID);
                    if($findNewUser){
                        $emailDdata['emailName']= $findNewUser->Name;
                        //$emailDdata['emailPage']= 'email-templates.test';
                        $emailDdata['emailPage']= 'email-templates.reset-password';
                        $emailDdata['emailTo']= $findNewUser->Email;
                        $emailDdata['emailSubject'] = 'Reset your password';

                        $emailDdata['name'] = $findNewUser->Name;
                        $emailDdata['email'] = $findNewUser->Email;
                        //$emailDdata['activelink'] = URL::to("app/auth/register?activate=".$doctorDetails->ActiveLink);
                        $emailDdata['activeLink'] = "<p>Please click <a href='".URL::to('app/auth/password-reset?resetcode='.$findNewUser->ResetLink)."'> This Link</a> to reset your password</p>";
                        $emailDdata['resetLink'] = URL::to('app/auth/password-reset?resetcode='.$findNewUser->ResetLink);
                        EmailHelper::sendEmail($emailDdata);
                    }
                }
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Forgot");
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("EmptyValues");
        }
        return $returnObject;

    }
    
    public static function UpdateUserProfile($updateArray){
        $user = new User();
        $updatedUser = $user->updateUserProfile($updateArray);
        if($updatedUser){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public static function FindUserProfile($profileid){
        $user = new User();
        $findUserProfile = $user->getUserProfile($profileid);
        if($findUserProfile){
            return $findUserProfile;
        }else{
            return FALSE;
        }
    }
    
    
    public static function ChangePassword($profileid){
        $allInputdata = Input::all();
        $returnObject = new stdClass();
        if(!empty($allInputdata)){
            $findUser = self::FindUserProfile($profileid);
            if($findUser){           
                if($findUser->Password == StringHelper::encode($allInputdata['oldpassword'])){
                    $updateArray['userid'] = $findUser->UserID;
                    $updateArray['Password'] = StringHelper::encode($allInputdata['password']);
                    $updateArray['updated_at'] = time();
                    $updated = self::UpdateUserProfile($updateArray);
                    if($updated){
                        $returnObject->status = TRUE;
                    }else{
                        $returnObject->status = FALSE;
                        $returnObject->message = StringHelper::errorMessage("Update");
                    }
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Tryagain");
                }
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Tryagain");
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("EmptyValues");
        }
        return $returnObject;
    }
    
    public static function AddDeviceToken($findUserID){
        $devicedetails = Input::all();
        $returnObject = new stdClass();
        if(!empty($devicedetails)){
            $findDeviceToken = self::FindDeviceToken($findUserID);
            if($findDeviceToken){
                //$updateArray['tokenid'] = $findDeviceToken->DeviceTokenID;
                $updateArray['Token'] = $devicedetails['device_token'];
                $updateArray['Device_Type'] = $devicedetails['device_type'];
                $updateArray['updated_at'] = time();
                $tokenUpdated = self::UpadeDeviceToken($updateArray,$findDeviceToken->DeviceTokenID);    
                if($tokenUpdated){
                    $returnObject->status = TRUE;
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Tryagain");
                }
            }else{
                $dataArray['userid'] = $findUserID;
                $dataArray['device_token'] = $devicedetails['device_token'];
                $dataArray['device_type'] = $devicedetails['device_type'];
                $insertToken = self::InsertDeviceToken($dataArray);
                if($insertToken){
                    $returnObject->status = TRUE;
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Tryagain");
                }
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("EmptyValues");
        }
        return $returnObject;
    }
    
    public static function InsertDeviceToken($insertArray){
        $devicetoken = new DeviceToken();
        $insertid = $devicetoken->AddDeviceToken($insertArray);
        if($insertid){
            return $insertid;
        }else{
            return FALSE;
        }
    }
    public static function FindDeviceToken($profileid){
        if(!empty($profileid)){
            $devicetoken = new DeviceToken();
            $getDetails = $devicetoken->GetDeviceToken($profileid);
            if($getDetails){
                return $getDetails;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }
    public static function UpadeDeviceToken($updateArray,$tokenid){
        if(!empty($updateArray)){
            $devicetoken = new DeviceToken();
            $updated = $devicetoken->UpdateDeviceToken($updateArray,$tokenid);
            if($updated){
                return TRUE;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }
    
    public static function DisableProfile($userid){
        $returnObject = new stdClass();
        $updateArray['userid'] = $userid;
        $updateArray['Active'] = 0;
        $profileBlock = self::UpdateUserProfile($updateArray);
        if($profileBlock){
            $returnObject->status = TRUE;
            $returnObject->message = StringHelper::errorMessage("BlockProfile");
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("Update");
        }
        return $returnObject;
    }
    
    public static function EmailExist($email){
        $user = new User();
        if(!empty($email)){
            $emailExist = $user->checkEmail($email);
            if($emailExist){
                return TRUE;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }
    public static function AddNewUser($inputArray){
        $user = new User();
        if(!empty($inputArray)){
            $userID = $user->authSignup($inputArray);
            if($userID){
                return $userID;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }
  
    public static function FindUserFromToken($token){
        $accesstoken = new OauthAccessTokens();
        $findUserID = $accesstoken->FindUserID($token);
        if($findUserID){
            return $findUserID;
        }else{
            return FALSE;
        }
    }
    
    /* Use : Used to update provide while booking slot or queue
     * Access : Public 
     * 
     */
    public static function OTPProfileUpdate($userid){
        $returnObject = new stdClass();
        $mobileno = Input::get('mobile_phone');
        $promocode = Input::get('promo_code');
        $otpChallenge = StringHelper::OTPChallenge();     
        if(Input::get('nric')) {
            $dataArray['updated_at'] = time();
            $dataArray['NRIC'] = Input::get('nric');
        }if(!empty($mobileno)) {
            $dataArray['updated_at'] = time();
            $dataArray['PhoneNo'] = $mobileno;
            $dataArray['OTPStatus'] = 0;
            $dataArray['OTPCode'] = $otpChallenge;
        }
        if(!empty($promocode)){
            $findPromoCode = General_Library::FindActivePromoCode($promocode);
            if($findPromoCode){
                $findUserPromoCode = General_Library::FindUserPromoCode($userid,$findPromoCode->PromoCodeID);
                if($findUserPromoCode){
                    //$returnObject->promostatus = FALSE;
                    $promostatus = 2;
                }else{
                    $promoArray['userid'] = $userid;
                    $promoArray['clinicid'] = 0;
                    $promoArray['promocodeid'] = $findPromoCode->PromoCodeID;
                    $promoArray['promocode'] = $promocode;
                    $userpromoid = General_Library::InsertUserPromoCode($promoArray);
                    if($userpromoid){
                        //$returnObject->promostatus = TRUE;
                        //$returnObject->status = TRUE;
                        $promostatus = 1;
                    }else{
                        //$returnObject->promostatus = FALSE;
                        //$returnObject->status = FALSE;
                        //$returnObject->message = StringHelper::errorMessage("PromoError");
                        $promostatus = 2;
                    }
                }
            }else{
                //$returnObject->promostatus = FALSE;
                //$returnObject->status = FALSE;
                //$returnObject->message = StringHelper::errorMessage("PromoError");
                $promostatus = 2;
            }
        }else{
            $promostatus = 0;
        }
        $dataArray['userid'] = $userid;
        //$dataArray['updated_at'] = time();
        $updateUserProfile = self::UpdateUserProfile($dataArray);
        if($updateUserProfile){  
            //Send OPT challenge
            if(!empty($mobileno)) {   
                $returnObject = self::SendOTPSMS($mobileno,$otpChallenge);
                if($returnObject==True && $promostatus !=2){
                    $returnObject->status = TRUE;
                }else{
                    //$returnObject = $returnObject;
                    if($promostatus==2){
                        $returnObject->status = FALSE;
                        $returnObject->message = StringHelper::errorMessage("PromoError");
                    }else{
                        $returnObject->status = FALSE;
                        $returnObject->message = StringHelper::errorMessage("OTPError");
                    }  
                }
            }else{
                if($promostatus != 2){
                    $returnObject->status = TRUE;
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("PromoError");
                }     
            }
        }else{
            if($promostatus==1){
                $returnObject->status = TRUE;      
            }elseif($promostatus==2){
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("PromoError");
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Update");
            }
        }
        return $returnObject;
    }
    
    
    
    public static function OTPCodeValidate($userid){
        $returnObject = new stdClass();  
        if(!empty(Input::get('otp_code'))) {
            $findOTP = self::FindOTPCode($userid,Input::get('otp_code'));
            if($findOTP){
                $updateArray['OTPStatus'] = 1;
                $updateArray['userid'] = $userid;
                $updateArray['updated_at'] = time();
                $updateUserProfile = self::UpdateUserProfile($updateArray);
                if($updateUserProfile){  
                    $returnObject->status = TRUE;
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Update");
                }
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("NoRecords");
            }
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("EmptyValues");
        }
        return $returnObject;
    }
    
    public static function FindOTPCode($userid,$otpcode){
        $user = new User();
        if(!empty($userid) && !empty($otpcode)){
            $findotpcode = $user->FindOTPCode($userid,$otpcode);
            if($findotpcode){
                return $findotpcode;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        } 
    }
    public static function OTPCodeResend($userid){
        $returnObject = new stdClass();  
        $otpChallenge = StringHelper::OTPChallenge();
        $updateArray['OTPStatus'] = 0;
        $updateArray['OTPCode'] = $otpChallenge;
        $updateArray['userid'] = $userid;
        $updateArray['updated_at'] = time();
        $updateUserProfile = self::UpdateUserProfile($updateArray);
        if($updateUserProfile){ 
            $findUserProfile = self::FindUserProfile($userid);
            //send otp again
            $returnObject = self::SendOTPSMS($findUserProfile->PhoneNo,$otpChallenge);
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("Update");
        } 
        return $returnObject;
    }
    /* Use : Send OTP SMS
     * Access : public
     * Parameter : mobile number and otp message
     */
    public static function SendOTPSMS($mobile,$otpcode){
        $returnObject = new stdClass();  
        $otpsmsSent = StringHelper::SendOTPSMS($mobile,$otpcode); 
        if($otpsmsSent){
            $returnObject->status = TRUE;
        }else{
            $returnObject->status = FALSE;
            $returnObject->message = StringHelper::errorMessage("OTPError");
        }  
        return $returnObject;
    }
    
    public static function FindUserProfileByRefID($refid){
        $user = new User();
        $findUserProfile = $user->UserProfileByRef($refid);
        if($findUserProfile){
            return $findUserProfile;
        }else{
            return FALSE;
        }
    }
}
