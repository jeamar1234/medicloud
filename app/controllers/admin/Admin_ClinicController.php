<?php

class Admin_ClinicController extends \BaseController {


	/* Use          :       Used to show all clinics details
     * Access       :       public
     * Return       :       view
     * 
     */
	public function getAllClinics()
    {
	 	$dataArray = array();
        $dataArray['title'] = "All Clinics"; 
        $getSessionData = AdminHelper::AuthSession();
        if($getSessionData !=FALSE){
            $clinic = new Admin_Clinic(); 
            $dataArray['resultSet'] = $clinic->GetClinicDetails();	
            return View::make('admin.manage-clinics',$dataArray);
        }else{
            return Redirect::to('admin/auth/login');
        }
    	
    }

    public function showAllClinic()
    {	
    	$allClinicsData = new Admin_Clinic();
	 	$selection = $allClinicsData->GetClinicDetails();
	 	$collection = Datatable::query($selection)
	        ->showColumns('ClinicID','Name', 'image','City','Country','Lat', 'Lng','Phone','Opening','ActiveCount','InactiveCount')
	        ->setSearchWithAlias()
	        ->searchColumns('ClinicID','Name')        
	        ->addColumn('Active',function($model)
	        {
	           if ($model->Active == 1)
			   {
					return 'Active';
			   } 
			   else
		   	   {
		   	   		return 'Inactive';
			   }
	        })
	        ->addColumn('Edit',function($model)
	        {
	            return "<a href=".$model->ClinicID."/edit class='btn btn-info'>Edit</a>" ;
	        })
	        ->addColumn('Set Time',function($model)
	        {
	            return "<a href=".$model->ClinicID."/new-time class='btn btn-info'>Set Time</a>" ;
	        })
	        ->make();
		return $collection;
    }
	/* Use          :       Used to add new clinic details
     * Access       :       public
     * Return       :       
     * 
     */
	public function AddNewClinic(){
            $getSessionData = AdminHelper::AuthSession();
            if($getSessionData !=FALSE){
            $dataArray = array();
            $dataArray['title'] = "Add a new clinic";		
            $insuranceCompanyData = new Admin_Insurance_Company();
            $clinicType = new Admin_Clinic_Type();
            $dataArray['company'] = $insuranceCompanyData->GetInsuranceCompanyList();
            $dataArray['clinicTypes'] = $clinicType->GetClinicTypes();
            return View::make('admin.add-clinic', $dataArray);
            }else{
                return Redirect::to('admin/auth/login');
            }
	}



	/* Use          :       Used to insert new clinic details
     * Access       :       public
     * Return       :       none
     * 
     */
	public function InsertClinic()
	{
		$validate = Validator::make(Input::all(), 

			array(
					'name'=>'required',
					'address'=>'required',
					'file'=>'image',
					'city'=>'required',
					'country'=>'required',
					'postal'=>'required',
					'latitude'=>'required',
					'longitude'=>'required',
					'phone'=>'required',
					'opening'=>'required',					
					'email' => 'required|email|unique:user',
					'password' => 'required | min:6',
					'clinic_price'=>'image'
					
				),
			array(
					'name.required'=>'Name is required'					
				)
			);

		if ($validate->fails()) {
			Input::flash();
			return Redirect::to('admin/clinic/new-clinic')->withErrors($validate);
		}
		else
		{			
			$clinicData = new Admin_Clinic();
			$clinicId = $clinicData->AddClinic();

			$clinicTypeId               = Input::get('clinic_type');
			if(!empty($clinicTypeId))
			{
				$clinicTypeDetail = new Admin_Clinic_Types_Detail();
				$clinicTypeDetail->AddClinicTypeDetail($clinicTypeId, $clinicId);
			}
			// Insert user details
			$userData = new Admin_User();			
			
			$insuranceID               = array();
			$insuranceID               = Input::get('insurance_company');

			if (!empty(Input::get('email')))
			{
				
				$userData->Ref_ID  = $clinicId;												
				$allData = $userData->AddUser();

				// if($clinicId)
				// {
				// 	//Send email when add new clinic
	   //              $clinicDetails = Admin_User::find($clinicId);
	   //              $emailDdata['emailName']= $clinicDetails->Name;
	   //              //$emailDdata['emailPage']= 'email-templates.test';
    //                 $emailDdata['emailPage']= 'email-templates.new-clinic';
	   //              $emailDdata['emailTo']= $clinicDetails->Email;
	   //              $emailDdata['emailSubject'] = 'Login credentials in your clinic';
	   //              $emailDdata['email'] = $clinicDetails->Email;
	   //              $emailDdata['password'] = Input::get ('password');
	   //              EmailHelper::sendEmail($emailDdata);
				// }		
			}

			if (!empty($insuranceID))
			{				
				foreach($insuranceID as $id)
				{
					$clinicInsuranceData = new Admin_Clinic_Insurance_Company();
					$clinicInsuranceData->AddClinicInsuranceData($id, $clinicId);						
				}				
			}


		}		
					
		return Redirect::to('admin/clinic/all-clinics');
	}



	/* Use          :       Used to edit clinic details
     * Access       :       public
     * Return       :       view
     * 
     */
	public function EditClinic($id)
	{	
        $getSessionData = AdminHelper::AuthSession();
        if($getSessionData !=FALSE){
		$dataArray = array();
		$dataArray['title'] = "Edit clinic";
		$insuranceCompanyData = new Admin_Insurance_Company();		
		$dataArray['company'] = $insuranceCompanyData->GetInsuranceCompanyList();
		$dataArray['insuranceID'] = $insuranceCompanyData->InsuranceCompanyByID($id);
		$clinicType = new Admin_Clinic_Type();
		$dataArray['clinicTypes'] = $clinicType->GetClinicTypes();
		$clinicTypeDetail = new Admin_Clinic_Types_Detail();
		$dataArray['cliniTypeID'] = $clinicTypeDetail->ClinicTypeByID($id);
		// get the clinic details by id
		$clinic = Admin_Clinic::find($id);
		// get the user details by id		
		$user 	= Admin_User::find($id);
		// show the edit form and pass the clinic
		return View::make('admin.edit-clinics', $dataArray)
		->with('clinic', $clinic)
		->with('user', $user);
             }else{
                return Redirect::to('admin/auth/login');
            }
	}



	/* Use          :       Used to update clinic details
     * Access       :       public
     * Return       :       view
     * 
     */
	public function UpdateClinic($id)
	{	
		$validate = Validator::make(Input::all(), 

			array(
				'name'=>'required',
				'file'=>'image',
				'email' => 'email|unique:user,Email,'.$id.',Ref_ID',
				'password' => 'min:6',
				'clinic_price'=>'image'

				
				),
			array(
				'name.required'=>'Name is required'				
				)
			);
                $findEmailUser = Admin_User::FindUserByEmail(Input::get('email'));
                $foundEmail = 0;
                if($findEmailUser){
                    if($findEmailUser->Ref_ID != $id){
                        $foundEmail = 1;
                    }
                }
		if ($validate->fails() || $foundEmail==1)

		//if ($validate->fails())

		{
			Input::flash();
			return Redirect::to('admin/clinic/'.$id.'/edit')
			       ->withErrors($validate)
			       ->withInput(Input::except('file'))		       	   
		       	   ->withInput(Input::except('password'));
		}
		else
		{
			//Update clinic details
			$data 			     = Admin_Clinic::find($id);
			$data->Custom_title	 = Input::get('custom_title');
			$data->Name          = Input::get('name');
			$data->Description   = Input::get('description');
			$data->Website  	 = Input::get('website');
			$data->Address       = Input::get('address');
			$data->City          = Input::get('city');            
			$data->State         = Input::get('state');            
			$data->Country       = Input::get('country');            
			$data->Postal        = Input::get('postal');            
			$data->District      = Input::get('district');            
			$data->Lat           = Input::get('latitude');  
			$data->Lng           = Input::get('longitude');
			$data->Phone         = Input::get('phone');            
			$data->MRT           = Input::get('mrt');            
			$data->Opening       = Input::get('opening'); 
			$data->Active        = Input::get('status'); 
	        
	        // Update user details
			$userData             = Admin_User::find($id);
			if (!empty($userData))
			{
				$userData->Email      = Input::get('email');
				$passwordVal		  = Input::get('password');

				if(!empty($passwordVal))
				{
					$userData->Password   = StringHelper::encode(Input::get('password'));					
				}
				else
				{
					Input::except('password');
				}			
	    		
	    		$userData->save();
	          			
			}
		 	else
           	{		$userDetails = new Admin_User();
	           		$userDetails->AddUser();
           	}
			
			//check image
		 	if(Input::hasFile('file'))
		 	{
	            $upload = Image_Library::CloudinaryUpload();			
	            $data->image = $upload;
	            $data->save();	           
	        }
	        else
	        {
	        	$data->save();
	        	if (!empty($userData))
				{	                    
		        	$userData->save();
	        	}
	        }

	        if(Input::hasFile('clinic_price'))
		 	{
		 		$uploadClinicPrice = Image_Library::CloudinaryUploadFile(Input::file('clinic_price'));			
	            $data->Clinic_Price = $uploadClinicPrice;
	            $data->save();
	        }


	        //Update clinic types
	        $clinicTypeId               = Input::get('clinic_type');
	        $clinicId               	= Input::get('clinicid');
	        $clinicTypeDetail = new Admin_Clinic_Types_Detail();
			if(!empty($clinicTypeDetail->ClinicTypeByID($id)))
			{
				$clinicTypeDetail = new Admin_Clinic_Types_Detail();
				$clinicTypeDetail->UpdateClinicTypeDetails($id, $clinicTypeId);
			}
			else
			{
				$clinicTypeDetail = new Admin_Clinic_Types_Detail();
				$clinicTypeDetail->AddClinicTypeDetail($clinicTypeId, $clinicId);
			}

	        //Update insurance company details	        
			$insuranceID = Input::get('insurance_company');		
			$insuranceIDCount = count($insuranceID);			

			if($insuranceIDCount > 0)
			{				
				$insuranceStatus = new Admin_Clinic_Insurance_Company();
				$insuranceStatus->UpdateInsurenceStatusInactive($id);

				foreach($insuranceID as $insuranceIDNew)
				{	
			
					$insuranceValues = new Admin_Clinic_Insurance_Company();
					$checkValues = $insuranceValues->GetInsurenceCompanyDetails($id, $insuranceIDNew);

					//check values
					if(is_null($checkValues))
					{
						$insuranceData = new Admin_Clinic_Insurance_Company();
						$insuranceData->AddClinicInsuranceData($insuranceIDNew,$id);				
					}
					else
					{
						$insuranceData = new Admin_Clinic_Insurance_Company();
						$insuranceData->UpdateInsurenceStatusActive($id,$insuranceIDNew);										
					}
				}

			}
			else
			{
				
			}

            // redirect
			Session::flash('message', 'Successfully updated');
			return Redirect::to('admin/clinic/all-clinics');
		} 
	}

	/* Use          :       Used to show clinic time set interface
     * Access       :       public
     * Return       :       view
     * 
     */
	public function AddClinicTime($id)
	{
			$dataArray = array();
            $dataArray['title'] = "Add Time"; 
            $getSessionData = AdminHelper::AuthSession();
            if($getSessionData !=FALSE){                

                $clinic = Admin_Clinic::find($id);
                $clinicData = Admin_Clinic_Time::find($id);

                $clinicTime = new Admin_Clinic_Time();
				$dataArray['clinicTime'] = $clinicTime->GetClinicTime($id);
                //show the edit form and pass the clinic id	
                return View::make('admin.add-clinic-time', $dataArray)
                ->with('clinic', $clinic)
                ->with('clinicTimeData', $clinicData);
            }else{
                return Redirect::to('admin/auth/login');
            }
	}

 	public function InsertClinicTimeDetails()
    {
    	// Setup the validator
		// $rules = array('startTime' => 'required', 'endTime' => 'required');
		// $validator = Validator::make(Input::all(), $rules);

		// // Validate the input and return correct response
		// if ($validator->fails())
		// {	
		//     return Response::json(array(
		//         'success' => false,
		//         'errors' => $validator->getMessageBag()->toArray()

		//     ), 400); // 400 being the HTTP code for an invalid request.
		// }
		// return Response::json_encode(array('success' => true), 200);

			$returnValues           = array();
        	$returnValues['status'] = "";
        	$returnValues['msg']    = "";
	 		
    	 if(Request::ajax()){
			$clinicTimeData = new Admin_Clinic_Time();
			$clinicTimeData->ClinicID = Input::get("clinicID");
			$clinicTimeData->StartTime = Input::get("clinicStartDate");
			$clinicTimeData->EndTime = Input::get("clinicEndDate");
			$clinicTimeData->Mon = Input::get("clinicDateMon");
			$clinicTimeData->Tue = Input::get("clinicDateTue");
			$clinicTimeData->Wed = Input::get("clinicDateWed");
			$clinicTimeData->Thu = Input::get("clinicDateThu");
			$clinicTimeData->Fri = Input::get("clinicDateFri");
			$clinicTimeData->Sat = Input::get("clinicDateSat");
			$clinicTimeData->Sun = Input::get("clinicDateSun");
			$clinicTimeData->Active = '1';

			// remove spaces between AM/PM 
			$removeSpaceStartTime = Input::get("clinicStartDate");
			$clinicStartTime = str_replace(' ', '', $removeSpaceStartTime);
			$removeSpaceEndTime = Input::get("clinicEndDate");
			$clinicEndTime = str_replace(' ', '', $removeSpaceEndTime);
			// $clinicTimeData->save();
		 	$clinicTimeId = DB::table('clinic_time')->insertGetId(
	 		array(
			 		'ClinicTimeID'=>$clinicTimeData,
			 		'ClinicID'=>Input::get("clinicID"),
			 		'StartTime'=>$clinicStartTime,
			 		'EndTime'=>$clinicEndTime,
			 		'Mon'=>Input::get("clinicDateMon"),
			 		'Tue'=>Input::get("clinicDateTue"),
			 		'Wed'=>Input::get("clinicDateWed"),
			 		'Thu'=>Input::get("clinicDateThu"),
			 		'Fri'=>Input::get("clinicDateFri"),
			 		'Sat'=>Input::get("clinicDateSat"),
			 		'Sun'=>Input::get("clinicDateSun"),
			 		'Created_on'=>time(),
			 		'created_at'=>time(),
			 		'updated_at'=>'0',			 		
			 		'Active'=>'1'
	 		));
			
			// $response = array(
   //          	'status' => 'success',
   //          	'msg' => 'Time set successfully',
   //          	'StartDate' => Input::get('clinicStartDate')
		 //        );
		        // return Response::json($response);
		 	$returnValues['status']        = "success";
            $returnValues['msg']           = "Time Set Successfully Completed.";            
            $returnValues['clinicTimeId']  = $clinicTimeId;
            $returnValues['startTime']     = Input::get("clinicStartDate");
            $returnValues['endTime'] 	   = Input::get("clinicEndDate");            
            $returnValues['monday'] 	   = Input::get("clinicDateMon");            
            $returnValues['tuesday'] 	   = Input::get("clinicDateTue");            
            $returnValues['wednesday'] 	   = Input::get("clinicDateWed");            
            $returnValues['thursday'] 	   = Input::get("clinicDateThu");            
            $returnValues['friday'] 	   = Input::get("clinicDateFri");            
            $returnValues['saturday'] 	   = Input::get("clinicDateSat");            
            $returnValues['sunday'] 	   = Input::get("clinicDateSun");            
            $returnValues['active'] 	   = '1';            

	     	echo json_encode ($returnValues);

		    }else{

	         	$returnValues['status'] = "error";
            	$returnValues['msg']    = "Time Set Unsuccessfull.";

        		echo json_encode ($returnValues);
		    }

		
    }

    public function EditTimeSchedule($id)
    {
    	$getSessionData = AdminHelper::AuthSession();
        if($getSessionData !=FALSE){
		$dataArray = array();
		$dataArray['title'] = "Edit Time Schedule";

		$clinic = Admin_Clinic::find($id);		
		$clinicTimeData = new Admin_Clinic_Time();
		$dataArray['clinicTime'] = $clinicTimeData->GetClinicTimeIdDetails($id);		
		return View::make('admin.edit-clinic-time', $dataArray);		
     	}else{
            return Redirect::to('admin/auth/login');
        }
    }

    public function UpdateTimeSchedule($id)
    {
    	$getSessionData = AdminHelper::AuthSession();
        if($getSessionData !=FALSE){		
			//Update clinic time details
        	$clinicID = Input::get('clinicid');
        	$clinictimeid = Input::get('clinictimeid');
         	
			$startTime  = Input::get('startTime');
			$endTime   	= Input::get('endTime');						
			$monday     = Input::get('monday');
			$tuesday    = Input::get('tuesday');
			$wednesday  = Input::get('wednesday');
			$thursday   = Input::get('thursday');
			$friday     = Input::get('friday');
			$saturday   = Input::get('saturday');
			$sunday     = Input::get('sunday');
			$status     = Input::get('status');
			//update table 
			Admin_Clinic_Time::where('ClinicTimeID', $clinictimeid)->update(array(
	            'StartTime' =>  $startTime,
	            'EndTime'   =>  $endTime,
	            'Mon'       =>  $monday,
	            'Tue'       =>  $tuesday,
	            'Wed'       =>  $wednesday,
	            'Thu'       =>  $thursday,
	            'Fri'       =>  $friday,
	            'Sat'       =>  $saturday,
	            'Sun'       =>  $sunday,
	            'Active'    =>  $status            
        	));
            // redirect			
			return Redirect::to('admin/clinic/'.$clinicID.'/new-time');
		
     	}else{
            return Redirect::to('admin/auth/login');
        }
    }

    public function SearchBookingPage(){
        $clinic = new Admin_Clinic();
            $getAllClinics = $clinic->GetAllClinics();
            //echo '<pre>'; print_r($getAllClinics); echo '</pre>';
            
        $getSessionData = AdminHelper::AuthSession();
        if($getSessionData !=FALSE){
            $dataArray['title'] = "Search Booking Page";
            $dataArray['cliniclist'] = $getAllClinics;
            return View::make('admin.search_booking_page', $dataArray);
        }else{
            return Redirect::to('admin/auth/login');
        }
    }
    public function SearchBooking(){
        $inputdata = Input::all();
        $getSessionData = AdminHelper::AuthSession();
        if($getSessionData !=FALSE){
            $appoinment = new Admin_Appoinment();
            
            if($inputdata['bookingid']){
                $findAppoinments = $appoinment->BookingById($inputdata['bookingid']);
            }else{
                $starttime = strtotime($inputdata['startdate']);
                $endtime = strtotime($inputdata['enddate']);
                $findAppoinments = $appoinment->FindCustomBooking($starttime,$endtime,$inputdata['clinic'],$inputdata['doctor']);
            }
            if($findAppoinments){
                $returnObject['myloadArray'] = $findAppoinments;
                $view = View::make('admin.search_results', $returnObject);
                return $view;
            }else{
                return 0;
            }
            //$dataArray['title'] = "Search Booking Page";
            //return View::make('admin.search_booking_page', $dataArray);
        }else{
            return 0;
        }
    }
    public function ClinicDoctor(){
        $inputdata = Input::all();
        $getSessionData = AdminHelper::AuthSession();
        if($getSessionData !=FALSE){
            $docavailability = new Admin_Doctor_Availability();
            $findAllDoctors = $docavailability->FindAllClinicDoctors($inputdata['clinicid']);
            if($findAllDoctors){
                $returnObject['doctorlist'] = $findAllDoctors;
                $view = View::make('admin.doctor_results', $returnObject);
                return $view;
                //return $findAllDoctors;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    
}