<?php

use Illuminate\Support\Facades\Input;
//use Symfony\Component\Security\Core\User\User;
class Api_V1_AuthController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		echo "index";
	}

        
        public function Login(){
            $findLogin = AuthLibrary::Login();
            return Response::json($findLogin);
        }

        //Login by mobile app
	/*
        public function login_old(){
            $dataArray = array();
            $token = Authorizer::issueAccessToken();
            if($token){
                $dataArray['error']= "false";
                //$dataArray['status'] = TRUE;
                $dataArray['data'] = $token;
            }else{
                $dataArray['status'] = FALSE;
                $dataArray['message'] = StringHelper::errorMessage("Login");
            }     
            
            return Response::json($dataArray);
	}*/
        
        /*  Access      :   Public
            Function    :   User sign up from mobile 
            Parameter   :   User details
            Author      :   Rizvi
            Return      :   User id
            Updated     :   
        */
	public function Signup(){
            $returnObject = new stdClass();
            $returnObject = AuthLibrary::SignUp();
            
            return Response::json($returnObject);  
            
                 
	}
        
        /*  Access      :   Public
            Function    :   Update 
            Parameter   :   Input values
            Author      :   Rizvi
            Return      :   Json (True / False)
            Updated     :   
        */
        public function UpdateUserProfile(){
            $returnObject = new stdClass();
            $findUserID = AuthLibrary::validToken();
            if($findUserID){
                $returnObject = AuthLibrary::ProfileUpdate($findUserID);
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Token");
            }
            return Response::json($returnObject);
        }
        
        
        
        
        //Use for forgot password by Mobile app
        public function Forgot_Password(){
            $returnObject = new stdClass();
            $returnObject = AuthLibrary::Forgot_Password();
            //$returnObject->status = FALSE;
            //$returnObject->message = StringHelper::errorMessage("EmptyValues");
            return Response::json($returnObject);
        }
        
        
        
        //Use for forgot password by Mobile app
        public function Check_Email(){
            $email = Input::get ('email');
            $returnArray = array();
            if($email ==""){
                $returnArray['status'] = FALSE;
                $returnArray['message'] = StringHelper::errorMessage("EmailEmpty");
               // echo json_encode("Sorry! empty value");
                //return Response::json("Sorry! empty value");
            }else{
                $user = new User();
                $findUserEmail = $user->checkEmail($email);
                if($findUserEmail){
                    $returnArray['status'] = TRUE;
                    $returnArray['data']['userid'] = $findUserEmail;
     
                }else{
                    $returnArray['status'] = FALSE;
                    $returnArray['message'] = StringHelper::errorMessage("EmailExist");
                }
                
            }
            return Response::json($returnArray);
        }
         
        
        // Used to logout the user
        public function LogOut(){
            $AccessToken = new OauthAccessTokens();
            $returnObject = array();
            $getRequestHeader = StringHelper::requestHeader();
            if($getRequestHeader['Authorization']!=""){
                $token = $getRequestHeader['Authorization'];
                $getAccessToken = $AccessToken->FindToken($token);
                if($getAccessToken){      
                    $deleteToken = $AccessToken->DeleteToken($getAccessToken->id);
                    if($deleteToken == TRUE){
                        $returnObject['status'] = TRUE;
                        $returnObject['data']['message'] = StringHelper::errorMessage("logout");
                    }else{
                        $returnObject['status'] = FALSE;
                        $returnObject['message'] = StringHelper::errorMessage("logerror");
                    }
                    //return Response::json($returnObject);
                }
            }else{
                $returnObject['status'] = FALSE;
                $returnObject['message'] = StringHelper::errorMessage("Token");
            } 
            return Response::json($returnObject);
        }
        
        
        /* Use          :   Used to build user profile page
         * Access       :   Public
         * Parameter    :   Token
         * Return       :   User details by array
         */
        
        public function UserProfile(){
            $returnObject = new stdClass();
            $getUserID = $this->returnValidToken(); 
            if(!empty($getUserID)){
                $returnObject = $this->GetUserProfileInformation($getUserID);
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Token");
            }
            return Response::json($returnObject);
            
            
        /*    $AccessToken = new Api_V1_AccessTokenController();
            $getRequestHeader = StringHelper::requestHeader();
            $authSession = new OauthSessions();
            $returnObject = new stdClass();
            
            if($getRequestHeader['Authorization']==""){    
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Token");
            }else{
                $getAccessToken = $AccessToken->FindToken($getRequestHeader['Authorization']);
                if($getAccessToken){
                    $findUserID = $authSession->findUserID($getAccessToken->session_id);
                    if($findUserID){
                        $returnObject = $this->GetUserProfileInformation($findUserID);
                    }else{
                        $returnObject->status = FALSE;
                        $returnObject->message = StringHelper::errorMessage("Token");
                    }
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Token");
                }
            }
            return Response::json($returnObject);
         
         */
        }
        
        /* Use      :   Used to return valid token
         * Access   :   No public access is allowed
         * 
         */
        public function returnValidToken(){
            $AccessToken = new Api_V1_AccessTokenController();
            $authSession = new OauthSessions();
            $getRequestHeader = StringHelper::requestHeader();
            if(!empty($getRequestHeader['Authorization'])){
                $getAccessToken = $AccessToken->FindToken($getRequestHeader['Authorization']);
                if($getAccessToken){
                    $findUserID = $authSession->findUserID($getAccessToken->session_id);
                    if($findUserID){
                        return $findUserID;
                    }
                }
            }     
        }

        /* Use          :   Used to delete user allergy
         * By           :   Mobile 
         * Parameter    :   value
         */
        
        public function DeleteUserAllergy(){
            $userallergy = new UserAllergy();
            $allergyid = Input::get ('value');
            $returnObject = new stdClass();
            if($allergyid=="" || $allergyid ==null){
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("EmailEmpty");
            }else{
                $getUserID = $this->returnValidToken();
                $dataArray['allergyid'] = $allergyid;
                $dataArray['Active'] = 0;
                if(!empty($getUserID)){
                    $updateAllergy = $userallergy->updateUserAllergy($dataArray);
                    if($updateAllergy){
                        $returnObject->status = TRUE;
                    }else{
                        $returnObject->status = FALSE;
                        $returnObject->message = StringHelper::errorMessage("Update");
                    }
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Token");
                }      
            }
            return Response::json($returnObject);
        }
        
        /* Use          :   Used to delete user medical condition
         * By           :   Mobile 
         * Parameter    :   value
         */
        public function DeleteUserCondition(){
            $usercondition = new UserCondition();
            $conditionid = Input::get ('value');
            $returnObject = new stdClass();
            if($usercondition=="" || $usercondition ==null){
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("EmailEmpty");
            }else{
                $getUserID = $this->returnValidToken();
                $dataArray['conditionid'] = $conditionid;
                $dataArray['Active'] = 0;
                if(!empty($getUserID)){
                    $deleteCondition = $usercondition->updateUserCondition($dataArray);
                    if($deleteCondition){
                        $returnObject->status = TRUE;
                    }else{
                        $returnObject->status = FALSE;
                        $returnObject->message = StringHelper::errorMessage("Update");
                    }
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Token");
                }  
            }
            return Response::json($returnObject);
        }
        
        
        public function DeleteUserMedication(){
            $usermedication = new UserMedication();
            $medicationid = Input::get ('value');
            $returnObject = new stdClass();
            if($usermedication=="" || $usermedication ==null){
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("EmailEmpty");
            }else{
                $getUserID = $this->returnValidToken();
                $dataArray['medicationid'] = $medicationid;
                $dataArray['Active'] = 0;
                if(!empty($getUserID)){
                    $deleteMedication = $usermedication->updateUserMedication($dataArray);
                    if($deleteMedication){
                        $returnObject->status = TRUE;
                    }else{
                        $returnObject->status = FALSE;
                        $returnObject->message = StringHelper::errorMessage("Update");
                    }
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Token");
                }  
            }
            return Response::json($returnObject);
        }
        
        //Delete medical histroy
        //Parameter history id 
        //Return json
        public function DeleteMedicalHistory(){
            $medicalhistory = new UserMedicalHistory();
            $historydetails = new UserMedicalHistoryDetail();
            
            $historyid = Input::get ('value');
            $returnObject = new stdClass();
            if($historyid=="" || $historyid ==null){
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("EmailEmpty");
            }else{
                $getUserID = $this->returnValidToken();
                if(!empty($getUserID)){
                    $dataArray['historyid'] = $historyid;
                    $dataArray['Active'] = 0;
                    $deletedHistory = $medicalhistory->updateMedicalHistory($dataArray);
                    if($deletedHistory){
                        //delete history details
                        $detailArray['historyid'] = $historyid;
                        $detailArray['Active'] = 0;

                        $deletedDetails = $historydetails->updateHistoryDetails($detailArray);
                        //if($deletedDetails){
                            $returnObject->status = TRUE;
                        //}    
                    }else{
                        $returnObject->status = FALSE;
                        $returnObject->message = StringHelper::errorMessage("Update");
                    }
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Token");
                }    
            }
            return Response::json($returnObject);
        }
        

        //No public direct access allowed
        public function GetUserProfile($profileid){
            $user = new User();
            if($profileid == "" || $profileid == null){
                return FALSE;
            }else{           
                $findUserProfile = $user->getUserProfile($profileid); 
                return $findUserProfile;
            }   
        }
        
        //No public direct access allowed
        public function GetUserAllergies($profileid){
            $userallergy = new UserAllergy();
            if($profileid == "" || $profileid == null){
                return FALSE;
            }else{           
                $findUserAllergy = $userallergy->getUserAllergies($profileid); 
                return $findUserAllergy;
            }
        }
        
        //No public direct access allowed
        public function GetUserMedications($profileid){
            $usermedication = new UserMedication();
            if($profileid == "" || $profileid == null){
                return FALSE;
            }else{           
                $findUserMedication = $usermedication->getUserMedications($profileid); 
                return $findUserMedication;
            }
        }
        //No public direct access allowed
        public function GetUserConditions($profileid){
            $usercondition = new UserCondition();
            if($profileid == "" || $profileid == null){
                return FALSE;
            }else{           
                $findUserCondition = $usercondition->getUserConditions($profileid);
                return $findUserCondition;
            }
        }
        //No public direct access allowed
        public function GetUserMedicalHistory($profileid){
            $userhistory = new UserMedicalHistory();
            if($profileid == "" || $profileid == null){
                return FALSE;
            }else{           
                $findUserHistory = $userhistory->getUserMedicalHistory($profileid);
                return $findUserHistory;
            }
        }
        
        
        
        
        //No public direct access allowed
        public function GetUserProfileInformation($profileid){
            $userinsurancepolicy = new UserInsurancePolicy();
            $returnArray = new stdClass();
                $findUserProfile = $this->GetUserProfile($profileid);
                $findUserAllergy = $this->GetUserAllergies($profileid);
                $findUserMedication = $this->GetUserMedications($profileid);
                $findUserCondition = $this->GetUserConditions($profileid);
                $findMedicalHistory = $this->GetUserMedicalHistory($profileid);
                //print_r($findUserProfile);
                if($findUserProfile){
                    //$userPolicy = $userinsurancepolicy->getUserInsurancePolicy($findUserProfile->UserID);
                    $userPolicy = $userinsurancepolicy->FindUserInsurancePolicy($findUserProfile->UserID);
                    
                    $returnArray->status = TRUE;
                    //$returnArray->data['profile'] = $findUserProfile;
                    $returnArray->data['profile']['user_id'] = $findUserProfile->UserID;
                    $returnArray->data['profile']['email'] = $findUserProfile->Email;
                    $returnArray->data['profile']['full_name'] = $findUserProfile->Name;
                    //$returnArray->data['profile']['image'] = URL::to('/assets/upload/user/'.$findUserProfile->Image);
                    $returnArray->data['profile']['nric'] = $findUserProfile->NRIC;
                    $returnArray->data['profile']['fin'] = $findUserProfile->FIN;
                    $returnArray->data['profile']['mobile_phone'] = $findUserProfile->PhoneNo;
                    $returnArray->data['profile']['dob'] = $findUserProfile->DOB;
                    $returnArray->data['profile']['age'] = $findUserProfile->Age;
                    $returnArray->data['profile']['weight'] = $findUserProfile->Weight;
                    $returnArray->data['profile']['height'] = $findUserProfile->Height;
                    if(!empty($findUserProfile->Weight) && !empty($findUserProfile->Height)){
                        $bmi = $findUserProfile->Weight / (($findUserProfile->Height / 100) * ($findUserProfile->Height / 100));
                    }else {$bmi = 0; }
                    
                    $returnArray->data['profile']['bmi'] =  $bmi;
                    $returnArray->data['profile']['blood_type'] = $findUserProfile->Blood_Type;
                    //This one need to change when image upload available
                    $returnArray->data['profile']['photo_url'] = $findUserProfile->Image;
                    if($userPolicy){
                        $returnArray->data['profile']['insurance_company'] = $userPolicy->Name;
                        $returnArray->data['profile']['insurance_policy_no'] = $userPolicy->PolicyNo;
                        $returnArray->data['profile']['insurance_policy_name'] = $userPolicy->PolicyName;
                    }else{
                        $returnArray->data['profile']['insurance_company'] = null;
                        $returnArray->data['profile']['insurance_policy_no'] = null;
                        $returnArray->data['profile']['insurance_policy_name'] = null;
                    }
                    //Insurance details
                    if($userPolicy){                         
                        $returnArray->data['insurance']['insurance_id'] = $userPolicy->UserInsurancePolicyID;
                        $returnArray->data['insurance']['name'] = $userPolicy->Name; 
                        $returnArray->data['insurance']['policy_no'] = $userPolicy->PolicyNo;
                        $returnArray->data['insurance']['policy_name'] = $userPolicy->PolicyName;
                        $returnArray->data['insurance']['expire_date'] = 0;
                        //$returnArray->data['insurance']['image_url'] = URL::to('/assets/'.$userPolicy->Image);
                        $returnArray->data['insurance']['image_url'] = $userPolicy->Image;
                    }else{
                        $returnArray->data['insurance'] = null;
                    }

                    //Allagies 
                    if($findUserAllergy){
                        foreach($findUserAllergy as $allergy){
                            $getAllergy['allergy_id'] = $allergy->AllergyID;
                            $getAllergy['name'] = $allergy->Name;
                            $allAllergy[] = $getAllergy;
                        }
                        $returnArray->data['allergies'] = $allAllergy;
                    }else{
                        $returnArray->data['allergies'] = null;
                    }
                    
                    //Medications
                    if($findUserMedication){
                        foreach($findUserMedication as $medication){
                            $getMedication['medication_id'] = $medication->MedicationID;
                            $getMedication['name'] = $medication->Name;
                            $getMedication['dosage'] = $medication->Dosage;
                            $allMedication[] = $getMedication;
                        }
                        $returnArray->data['medications'] = $allMedication;
                    }else{
                        $returnArray->data['medications']= null;
                    }
                    
                    //Conditions
                    if($findUserCondition){
                        foreach($findUserCondition as $condition){
                            $getCondition['condition_id'] = $condition->ConditionID;
                            $getCondition['name'] = $condition->Name;
                            $newDate = date("d-m-Y", strtotime($condition->Date));
                            $getCondition['date'] = $newDate;
                            $allConditions[] = $getCondition;
                        }
                        $returnArray->data['conditions'] = $allConditions;
                    }else{
                        $returnArray->data['conditions'] = null;
                    }
                    
                    //History
                    if($findMedicalHistory){
                        foreach($findMedicalHistory as $history){
                            $historyDetail = new UserMedicalHistoryDetail();
                            $findHistoryDetail = $historyDetail->getUserMedicalHistoryDetails($history->HistoryID);
                            $newDate = date("d-m-Y", strtotime($history->Date));
                            $getHistory['date'] = $newDate;
                            $getHistory['record_id'] = $history->HistoryID;
                            $getHistory['visit_type'] = $history->VisitType;
                            //$getHistory['list']['doctor'] = $history->Name;
                            $getHistory['doctor'] = $history->Doctor_Name;
                            $getHistory['clinic_name'] = $history->Clinic_Name;
                            //need to check clinic
                            ///$getHistory['list']['clinic_name'] = $history->Name;
                            $getHistory['note'] = $history->Note;
                            //$getHistory['date'] = $newDate;
                            if($findHistoryDetail){
                                foreach($findHistoryDetail as $hisDetail){                                 
                                    $getHistoryDetail['attachment_id'] = $hisDetail->DetailID;
                                    $getHistoryDetail['url'] = URL::to('/assets/'.$hisDetail->Image);
                                    $getHistory1[] = $getHistoryDetail;
                                }
                                $getHistory['attachments'] = $getHistory1;
                            }else{
                                $getHistory['attachments'] = null;
                            }
                            $allHistory[] = $getHistory;
                        }
                        
                        $returnArray->data['history'] = $allHistory;
                    }else{
                        $returnArray->data['history'] = null;
                    }                  
                }else{
                    $returnArray->status = FALSE;
                    $returnArray->message = StringHelper::errorMessage("NoRecords");
                }
            return $returnArray;
        }

        //Add new user allergy
        public function AddNewAllergy(){
            $allergy = Input::get ('allergy');
            $userid = Input::get ('userid');
            
            $userallergy = new UserAllergy();
            $dataArray = array();
            $dataArray['allergy']= $allergy;
            $dataArray['userid']= $userid;
            
            $returnObject = new stdClass();
                
            $insertedID = $userallergy->insertAllergy($dataArray);
            if($insertedID){
                $returnObject->status = TRUE;
                $returnObject->data['record_id'] = $insertedID;
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Tryagain");
            }
            return Response::json($returnObject);  
	}
        
        //Add new user medical condition
        public function AddNewMedicalCondition(){
            $condition = Input::get ('condition');
            $userid = Input::get ('userid');
            $date = Input::get ('date');
            $newDate = date("d-m-Y", strtotime($date));
            
            $medicalcondition = new UserCondition();
            $dataArray = array();
            $dataArray['condition']= $condition;
            $dataArray['userid']= $userid;
            $dataArray['date']= $newDate;
            
            $returnObject = new stdClass();
                
            $insertedID = $medicalcondition->insertMedicalCondition($dataArray);
            if($insertedID){
                $returnObject->status = TRUE;
                $returnObject->data['record_id'] = $insertedID;
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Tryagain");
            }
            return Response::json($returnObject);  
	}
        
        //Add new user medication
        public function AddNewUserMedication(){
            $medication = Input::get ('medication');
            $userid = Input::get ('userid');
            $dosage = Input::get ('dosage');
            
            $usermedication = new UserMedication();
            $dataArray = array();
            $dataArray['medication']= $medication;
            $dataArray['userid']= $userid;
            $dataArray['dosage']= $dosage;
            
            $returnObject = new stdClass();
                
            $insertedID = $usermedication->insertUserMedication($dataArray);
            if($insertedID){
                $returnObject->status = TRUE;
                $returnObject->data['record_id'] = $insertedID;
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Tryagain");
            }
            return Response::json($returnObject);  
	}
        
        //Add new medical history
        public function AddNewMedicalHistory(){
            $visittype = Input::get ('visit_type');
            $userid = Input::get ('user_id');
            $doctor = Input::get ('doctor');
            $clinic = Input::get ('clinic_name');
            $note = Input::get ('note');
            $date = Input::get ('date'); 
            $newDate = date("d-m-Y", strtotime($date));
            
            $medicalhistory = new UserMedicalHistory();
            $dataArray = array();
            $dataArray['visittype']= $visittype;
            $dataArray['userid']= $userid;
            $dataArray['doctor']= $doctor;
            $dataArray['clinic']= $clinic;
            $dataArray['note']= $note;
            $dataArray['date']= $newDate;
            
            $returnObject = new stdClass();
                
            $insertedID = $medicalhistory->insertMedicalHistory($dataArray);
            if($insertedID){
                $returnObject->status = TRUE;
                $returnObject->data['record_id'] = $insertedID;
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Tryagain");
            }
            return Response::json($returnObject);  
	}
        
        public function UpdateMedicalHistory(){
            $allUpdatedata = Input::all();
            $userhistory = new UserMedicalHistory();
            //$insurancepolicy = new UserInsurancePolicy();
            $returnObject = new stdClass();

            if(is_array($allUpdatedata) && count($allUpdatedata) >0 ){
                if(!empty($allUpdatedata['visit_type'])) {
                    $dataArray['VisitType'] = $allUpdatedata['visit_type'];
                }if(!empty($allUpdatedata['doctor'])) {
                    $dataArray['Doctor_Name'] = $allUpdatedata['doctor'];
                }if($allUpdatedata['clinic_name']) {
                    $dataArray['Clinic_Name'] = $allUpdatedata['clinic_name'];
                }if(!empty($allUpdatedata['note'])) {
                    $dataArray['Note'] = $allUpdatedata['note'];
                }if(!empty($allUpdatedata['date'])) {
                    $dataArray['Note'] = date("d-m-Y", strtotime($allUpdatedata['date']));
                }
                $dataArray['updated_at'] = time();
                $dataArray['historyid'] = $allUpdatedata['history_id'];
                
                $updateHistory = $userhistory->updateMedicalHistory($dataArray);
                if($updateHistory){
                    $returnObject->status = TRUE;
                }else{
                    $returnObject->status = FALSE;
                    $returnObject->message = StringHelper::errorMessage("Update");
                }
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("EmailEmpty");
            }
            return Response::json($returnObject);
        }
        
        
        public function ChangePassword(){
            $returnObject = new stdClass();
            $findUserID = AuthLibrary::validToken();
            if($findUserID){
                $returnObject = AuthLibrary::ChangePassword($findUserID);
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Token");
            }
            return Response::json($returnObject);
        }
        
        public function AddDeviceToken(){
            $returnObject = new stdClass();
            $findUserID = AuthLibrary::validToken();
            if($findUserID){
                $returnObject = AuthLibrary::AddDeviceToken($findUserID);
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Token");
            }
            return Response::json($returnObject);
        }
        
        /* Use          :   Used to disable a user profile
         * Access       :   Public
         * 
         */
        public function DisableProfile(){   
            $returnObject = new stdClass();
            $findUserID = AuthLibrary::validToken();
            //$findUserID =1;
            if($findUserID){
                $returnObject = AuthLibrary::DisableProfile($findUserID);
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Token");
            }
            return Response::json($returnObject);
        }

        public function OTPProfileUpdate(){
            $returnObject = new stdClass();
            $findUserID = AuthLibrary::validToken();
            if($findUserID){
                $returnObject = AuthLibrary::OTPProfileUpdate($findUserID);
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Token");
            }
            return Response::json($returnObject);
        }
        public function OTPCodeValidation(){
            $returnObject = new stdClass();
            $findUserID = AuthLibrary::validToken();
            if($findUserID){
                $returnObject = AuthLibrary::OTPCodeValidate($findUserID);
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Token");
            }
            return Response::json($returnObject);
        }
        
        public function OTPCodeResend(){
            $returnObject = new stdClass();
            $findUserID = AuthLibrary::validToken();
            if($findUserID){
                $returnObject = AuthLibrary::OTPCodeResend($findUserID);
            }else{
                $returnObject->status = FALSE;
                $returnObject->message = StringHelper::errorMessage("Token");
            }
            return Response::json($returnObject);
        }
        
        
        
        
        
        
        public function FindCoordinate(){
            $response = Geocode::make()->address('#01-01 Blk 51 Avenue 3 Ang Mo Kio');

            if ($response) {
                echo $response->latitude()."<br>";
                echo $response->longitude()."<br>";
                echo $response->formattedAddress()."<br>";
                echo $response->locationType();
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//AuthLibrary::create();
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
