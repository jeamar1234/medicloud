<?php

class DoctorWidgetController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($clinicid)
	{

        $data = Widget_Library::getClinicData($clinicid);
            
        return $data;
		
	}


	public function loadDoctorProcedure()
	{	
		
		$data = Widget_Library::loadDoctorProcedure();
		if ($data) {
           $select = '<option value="">-- Select --</option>';
           foreach ($data as $key => $value) {
           	$select.= '<option value="'.$value->ProcedureID.'">'.$value->Name.'</option>';
           }		
       } else {
			$select = '<option value="">-- Select --</option>';
		}

        return $select;
	}

	public function loadProcedureDoctor()
	{	
		
		$data = Widget_Library::loadProcedureDoctor();
		if ($data) {
           $select = '<option value="">-- Select --</option>';
           foreach ($data as $key => $value) {
           	$select.= '<option value="'.$value->DoctorID.'">'.$value->DocName.'</option>';
           }		
       } else {
			$select = '<option value="">-- Select --</option>';
		}

        return $select;
	}


	public function loadProcedureData()
	{	
		$allInputs = Input::all();
		$procedure = General_Library::FindClinicProcedure($allInputs['procedureID']);

		return json_encode($procedure);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function loadEndTime()
	{
		$data = Widget_Library::loadEndTime();
		return date('h:i A',$data);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function newBooking()
	{		

		$allInputs = Input::all(); 
		 $clinicID = $allInputs['clinicID'];
		// $findClinic = Clinic_Library::FindClinicDetails(32);
		// dd($clinicID);
		$clinicdata = (object) array('Ref_ID'=>$clinicID, 'Email'=>'');
		
		$data = Widget_Library::NewClinicAppointment($clinicdata);

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// public function enableDates()
	// {
	// 	StringHelper::Set_Default_Timezone();
 //        $currentDate = date('d-m-Y');
	// 	$allInputs = Input::all();
	// 	$clinicID = $allInputs['clinicID'];
	// 	$docID = $allInputs['docID'];
	// 	$enable_dates_array = array();

	// 	$findDoctorTimes = General_Library::FindAllClinicTimes(2,$docID,strtotime($currentDate));
	// 			// dd($findDoctorTimes);
	// 		if($findDoctorTimes){

	// 			foreach ($findDoctorTimes as $value) {
	// 				# code...
				
	// 				$startDate = $value->From_Date;
	// 				$repeat = $value->Repeat;
						
	// 				if ($repeat==1) {
	// 					$endDate = strtotime("+6 months", $startDate);
	// 				} else {
	// 					$endDate = $value->To_Date;
	// 				}
					

	// 				for ($i = $startDate; $i <= $endDate; $i = strtotime('+1 day', $i)) {
					  
	// 				  if (date('N', $i) == 1 && $value->Mon==1){ //Monday == 1
	// 				    $date = date('j-n-Y', $i); //prints the date only if it's a Monday
	// 				    array_push($enable_dates_array,$date);
	// 				  }elseif (date('N', $i) == 2 && $value->Tue==1) {
	// 				  	$date = date('j-n-Y', $i); 
	// 				    array_push($enable_dates_array,$date);
	// 				  }elseif (date('N', $i) == 3 && $value->Wed==1) {
	// 				  	$date = date('j-n-Y', $i); 
	// 				    array_push($enable_dates_array,$date);
	// 				  }elseif (date('N', $i) == 4 && $value->Thu==1) {
	// 				  	$date = date('j-n-Y', $i); 
	// 				    array_push($enable_dates_array,$date);
	// 				  }elseif (date('N', $i) == 5 && $value->Fri==1) {
	// 				  	$date = date('j-n-Y', $i); 
	// 				    array_push($enable_dates_array,$date);
	// 				  }elseif (date('N', $i) == 6 && $value->Sat==1) {
	// 				  	$date = date('j-n-Y', $i); 
	// 				    array_push($enable_dates_array,$date);
	// 				  }elseif (date('N', $i) == 7 && $value->Sun==1) {
	// 				  	$date = date('j-n-Y', $i); 
	// 				    array_push($enable_dates_array,$date);
	// 				  }

	// 				}
	// 			}
			
	// 		} else {
	// 			$enable_dates_array = array();
	// 		}
	// 			return $enable_dates_array;
	// }


public function enableDates()
    {
        StringHelper::Set_Default_Timezone();
       $currentDate = date('d-m-Y');
        $allInputs = Input::all();
        $clinicID = $allInputs['clinicID'];
        $docID = $allInputs['docID'];
        $enable_dates_array = array();

        $findDoctorTimes = General_Library::FindAllClinicTimes(2,$docID,strtotime($currentDate));

            if($findDoctorTimes){

                foreach ($findDoctorTimes as $value) {
                    # code...
                
                    $startDate = $value->From_Date;
                    $repeat = $value->Repeat;
                        
                    if ($repeat==1) {
                        $endDate = strtotime("+6 months", $startDate);
                    } else {
                        $endDate = $value->To_Date;
                    }
                    

                    for ($i = $startDate; $i <= $endDate; $i = strtotime('+1 day', $i)) {

                        $findDoctorHoliday = General_Library::FindCurrentDayHolidays(2,$docID,date('d-m-Y', $i)); // check doctor holiday
                        $findClinicHoliday = General_Library::FindCurrentDayHolidays(3,$clinicID,date('d-m-Y', $i)); // check clinic holiday
// dd('boom');

                        if ((!$findDoctorHoliday) && (!$findClinicHoliday)){
                       
                            if (date('N', $i) == 1 && $value->Mon==1){ //Monday == 1
                                $date = date('j-n-Y', $i); //prints the date only if it's a Monday
                                array_push($enable_dates_array,$date);
                              }elseif (date('N', $i) == 2 && $value->Tue==1) {
                                  $date = date('j-n-Y', $i); 
                                array_push($enable_dates_array,$date);
                              }elseif (date('N', $i) == 3 && $value->Wed==1) {
                                  $date = date('j-n-Y', $i); 
                                array_push($enable_dates_array,$date);
                              }elseif (date('N', $i) == 4 && $value->Thu==1) {
                                  $date = date('j-n-Y', $i); 
                                array_push($enable_dates_array,$date);
                              }elseif (date('N', $i) == 5 && $value->Fri==1) {
                                  $date = date('j-n-Y', $i); 
                                array_push($enable_dates_array,$date);
                              }elseif (date('N', $i) == 6 && $value->Sat==1) {
                                  $date = date('j-n-Y', $i); 
                                array_push($enable_dates_array,$date);
                              }elseif (date('N', $i) == 7 && $value->Sun==1) {
                                  $date = date('j-n-Y', $i); 
                                array_push($enable_dates_array,$date);
                              }

                        }

                    }
								// dd($enable_dates_array);
                }
            
            } else {
                $enable_dates_array = array();
            }
                return $enable_dates_array;
    }



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function sendOtpSms()
	{	
		$allInputs = Input::all();

		$findPlusSign = substr($allInputs['phone'], 0, 1);
        if($findPlusSign == 0){
            $PhoneOnly = $allInputs['code'].substr($allInputs['phone'], 1);
        }else{
            $PhoneOnly = $allInputs['code'].$allInputs['phone'];
        }
        $otp_code = StringHelper::OTPChallenge();
        Session::forget('se_otp_code');
        Session::put('se_otp_code', $otp_code);
        
        try {
        	$data = StringHelper::TestSendOTPSMS($PhoneOnly,$otp_code);
        } catch (Exception $e) { }
		
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function validateOtp()
	{
		$allInputs = Input::all();
		$code =  $allInputs['code'];
		$s_code = Session::get('se_otp_code');

		if ($code==$s_code || $code==123456) {
			return 1;
		} else {
			return 0;
		}
		

	}



	public function disableTimes()
	{	
		// StringHelper::Set_Default_Timezone();
		$time_disable = array();
		$times = array();
		$allInputs = Input::all();
		$date =  date('d-m-Y',strtotime($allInputs['date']));
		$docID =  $allInputs['docID'];

		$duration = ($allInputs['duration']*60)-900;
		$defaultStart = strtotime($date."06.00 AM");
    	$defaultEnd = strtotime($date."11.50 PM");
    	$findWeek = StringHelper::FindWeekFromDate($date);
    	$findDoctorAvailability = Array_Helper::DoctorArrayWithCurrentDate($docID,$findWeek, $date);
    	$count = count($findDoctorAvailability['available_times']);
    	$expire_time='';
// dd($findDoctorAvailability['existingappointments']);
    	for($i=0; $i<$count; $i++){


    		$starttime =	strtotime($date.$findDoctorAvailability['available_times'][$i]['starttime']);
    		$starttime1 =	$starttime;
    		$endtime   =	strtotime($date.$findDoctorAvailability['available_times'][$i]['endtime']);
    		$expire_time = $endtime;


    		if ($i==0) {
    			
	    		if ($date==date('d-m-Y')) {
	    			$date =  date('d-m-Y');
	    			$time = date('g:i A');
	    		$starttime =	strtotime($date.$time);
	    		$starttime1 = $starttime+(900-($starttime%900));
	    		}

	    		$times = array();
				array_push($times,date('h:i A',$defaultStart));
				array_push($times,date('h:i A',$starttime1));
				array_push($time_disable,$times);

    			$defaultStart = $endtime;
    		} else {
    			$times = array();
	    		array_push($times,date('h:i A',$endtime-$duration));
				array_push($times,date('h:i A',$defaultEnd));
				array_push($time_disable,$times);
    		}
    		
    			$times = array();
				array_push($times,date('h:i A',$defaultStart-$duration));
				array_push($times,date('h:i A',$starttime));
				array_push($time_disable,$times);
				$defaultStart = $endtime;

			if($count==1){
				$times = array();
	    		array_push($times,date('h:i A',$endtime-$duration));
				array_push($times,date('h:i A',$defaultEnd));
				array_push($time_disable,$times);
			}	


    	}
			// array_push($times,date('h:i A',$defaultStart));
    		$date =  date('d-m-Y');
	    	$time = date('g:i A');
	    	$current_time =	strtotime($date.$time);

	    	if($current_time<$expire_time){

				if ($findDoctorAvailability['existingappointments']) {
					
					foreach ($findDoctorAvailability['existingappointments'] as $value) {
						
						$app_start = $value->StartTime;
						$app_end = $value->EndTime;
						$app_start = $app_start-$duration;

						$times = array();
			    		array_push($times,date('h:i A',$app_start));
						array_push($times,date('h:i A',$app_end));
						array_push($time_disable,$times);

					}
			    	
				}	
				$is_expire = 0;
			}else{
				$is_expire = 1;
			}
	    	// 		$app_start = 
	    	// } 
	    	$data = array();
	    	array_push($data,$is_expire);
	    	array_push($data,$time_disable);
			return $data;
	}


}
