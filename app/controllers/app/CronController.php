<?php
use Illuminate\Support\Facades\Input;

class CronController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		echo "index";
	}
        
        /* Use          :   Used to notify user on the day of session
         * Access       :   Cron
         * Parameter    :   
         */
        public function RemindAppointment(){
            CronLibrary::RemindAppointment();
            //Insurance_Library::RemindAppointment();
        }
        
        /* Use          :   Used to notify user 3 hours before the session
         * Access       :   Cron
         * Parameter    :
         */
        public function RemindAppointmentInHours(){
            CronLibrary::RemindAppointmentInHours();
            //Insurance_Library::RemindAppointmentInHours();
        }

        /* Use          :   Used to nitify the user 30 minutes before 
         * Access       :   Cron
         * Parameter    :   
         */
        public function RemindAppointmentInMinutes(){
            CronLibrary::RemindAppointmentInMinutes();
            //Insurance_Library::RemindAppointmentInMinutes();
        }
        /* Use          :   Used to diactivate all booing at night 11.59PM 
         * Access       :   Cron
         * Parameter    :   
         */
        public function DiactivateBookings(){
            CronLibrary::DiactivateBookings();
            //Insurance_Library::RemindAppointmentInMinutes();
        }

        /* Use          :   Used to notify user one day before the appointment 
         * Access       :   Cron
         * Parameter    :   
         * For          :   Direct SMS
         */
        public function SMSAppointmentBeforeDay(){
            CronLibrary::SMSAppointmentBeforeDay();
            //Insurance_Library::RemindAppointment();
        }

        /* Use          :   Used to notify user one hour before the appointment 
         * Access       :   Cron
         * Parameter    :   
         * For          :   Direct SMS
         */
        public function SMSAppointmentBeforeHour(){
            CronLibrary::SMSAppointmentBeforeHour();
            //Insurance_Library::RemindAppointment();
        }
        
   // NHR 2016-3-25
// Delete past google events 
        public function deleteGoogleEvent()
        {
            CronLibrary::deleteGoogleEvent();
        } 
        
        
        /* Use          :   Used to Inset diagnosis by doctor
     * Access       :   Ajax public
     * Parameter    :   
     */
    public function CronTest(){
        $emailDdata['emailName']= "Rizvi";
        $emailDdata['emailPage']= 'email-templates.test';
        $emailDdata['emailTo']= "nwnhemantha@gmail.com";
        $emailDdata['emailSubject'] = 'Email test by Cron';

        $emailDdata['name'] = "Raja";
        $emailDdata['email'] = "nwnhemantha@gmail.com";
        $emailDdata['activelink'] = "<p>Please click <a href='#'> This Link</a> to complete your registration</p>";
        EmailHelper::sendEmail($emailDdata);  
    }
    
  
    






                
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	

}
