@include('admin.header-admin') 
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/plug-ins/1.10.6/integration/bootstrap/3/dataTables.bootstrap.css">
<div class="row">
	<div class="page-header col-md-3 col-md-offset-1">		
		<h1 style="font-size: 100% !important;"><span class="label label-default"> All Clinics</span></h1>
	</div>
	<div class="col-md-6">
		<h5>{{ HTML::link('admin/clinic/search_booking', 'Search Booking')}} | {{ HTML::link('admin/clinic/new-clinic', 'Add New Clinic')}} | {{ HTML::link('admin/clinic/new-doctor', 'Add New Doctor')}} | {{ HTML::link('admin/clinic/all-doctors', 'All Doctors')}} | {{ HTML::link('admin/auth/logout', 'Logout')}} </h5>
	</div>
</div>
<div class="row">       
	<div class="col-md-12">
		<table class="table table-striped table-bordered"  id="allClinics" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Image</th>					
					<th>City</th>					
					<th>Country</th>					
					<th>Lat</th>					
					<th>Lng</th>					
					<th>Phone</th>					
					<th>Opening</th>					
					<th>Doctors Active Count</th>					
					<th>Doctors Inactive Count</th>					
					<th>Status</th>					
					<th>Action</th>					
					<th>Time</th>					
				</tr>	                        
			</thead>
		</table>  	 
	</div>
</div>
@include('admin.footer-admin')