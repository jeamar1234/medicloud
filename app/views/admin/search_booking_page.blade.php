@include('admin.header-admin') 
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">     

$(function() {
    $("#startdate").datepicker({
        dateFormat: "d-m-yy"
    });  
    $("#enddate").datepicker({
        dateFormat: "d-m-yy"
    });
  });
  </script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/plug-ins/1.10.6/integration/bootstrap/3/dataTables.bootstrap.css">
<div class="row">
	<div class="page-header col-md-3 col-md-offset-1">		
		<h1 style="font-size: 100% !important;"><span class="label label-default"> Search Booking</span></h1>
	</div>
	<div class="col-md-6">
		<h5>{{ HTML::link('admin/clinic/new-clinic', 'Add New Clinic')}} | {{ HTML::link('admin/clinic/new-doctor', 'Add New Doctor')}} | {{ HTML::link('admin/clinic/all-doctors', 'All Doctors')}} | {{ HTML::link('admin/auth/logout', 'Logout')}} </h5>
	</div>
</div>
<div class="row">       
	<div class="col-md-12">
            <div class="search-area">
                <div>
                    
                    <input class="search_option" type="radio" name="searchradio" checked="checked" value="0">
                    <label>Search by Booking id</label>
                    <br>
                    <input class="search_option" type="radio" name="searchradio" value="1">
                    <label>Advance search</label>
                </div>
                <div id="search-book-id">
                    <div>Search by booking id</div>
                    <div>
                        <label>Enter booking id</label> 
                        <input id="bookingid" type="text" name="bookingid">
                    </div>
                </div>
                <div id="search-custom">
                    <div>Custom Search</div>
                    <div>
                        <label>Start date</label>
                        <input id="startdate" type="text" name="startdate">
                        <label>End date</label>
                        <input id="enddate" type="text" name="enddate">
                        <label>Select clinic</label>
                        <select id="clinic" name="clinic" style="width: 250px">
                            <option value="">Select</option>
                            <?php if($cliniclist){
                                foreach($cliniclist as $clinicli){
                                    echo '<option value="'.$clinicli->ClinicID.'">'.$clinicli->Name.'</option>';
                                }    
                            }?>
                        </select>
                        <label>Select Doctor</label>
                        <select id="doctor" name="doctor" style="width: 250px">
                            <option value="">select</option>
                        </select>
                    </div>                   
                </div>
                <div id="search_booking" class="btn-booknow">Search Now</div>    
            </div>
            <div id="results-area"></div>
            
<!--            <table class="table table-striped table-bordered"   cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Image</th>					
					<th>City</th>					
					<th>Country</th>					
					<th>Lat</th>					
					<th>Lng</th>					
					<th>Phone</th>					
					<th>Opening</th>					
					<th>Doctors Active Count</th>					
					<th>Doctors Inactive Count</th>					
					<th>Status</th>					
					<th>Action</th>					
					<th>Time</th>					
				</tr>	                        
			</thead>
		</table> -->
</div>
@include('admin.footer-admin')