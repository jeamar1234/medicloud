<!DOCTYPE html>
<html>
<head>
	<title> {{ $title }}</title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito">

{{ HTML::style('assets/css/medicloudv3.css') }}
{{ HTML::style('assets/css/datepicker-appointment.css') }}
<!-- Latest compiled and minified CSS -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-2.2.0.min.js" ></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.min.js" ></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
{{ HTML::script('assets/js/doctor-widget.js') }}
{{ HTML::script('assets/js/jquery-timepicker/jquery.timepicker.js') }}

{{ HTML::style('assets/js/jquery-timepicker/jquery.timepicker.css') }}

<style type="text/css">
body { background-color: #eeeeee; }
.box { border: 1px solid #fff; margin: auto; background-color: white; padding: 30px; }
.borderless tbody tr td, .borderless tbody tr th, .borderless thead tr th { border: none; }
/*.table{ margin-left: 20px;}*/
.field-name { color: #C0C0C0; }
.btn-cancel, .btn-update { border: none !important; }
.ui-timepicker-wrapper ul.ui-timepicker-list li.ui-timepicker-disabled { display: none; }
.clear { clear: both; }
/*widget css added by Irshad 
	  Note: Alignment fixes made to the widget.
	  Dated:04 March 2016    */
	
	
.custom-float-l { float: left; }
.custom-float-r { float: right; }
.custom-padding-r { padding-right: 0px; }
.custom-padding-t { padding-top: 10px; }
.custom-margin-t0 { margin-top: 0px !important; }
.custom-padding-b { padding-bottom: 10px !important; }
.custom-margin-l { margin-left: 10px; }
.ui-datepicker { border: 1px solid #f0f0f0 !important; }
.error { padding-left: 10px !important; }
td { padding: 8px 8px 8px 8px !important; }
hr { margin-bottom: 12px; margin-top: 12px; }
.mobile-width2 { width: 46%; }

/*iPhone 2G-4S in portrait & landscape*/
@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
body { /*background-color:#F00;  height:100vh;*/ } /*red*/
}
																				 
																				 
																				

/*iPhone 5 & 5S in portrait & landscape*/
@media only screen and (min-device-width : 320px) and (max-device-width : 568px) {
body { background-color: #eeeeee; /* height:100vh;*/ }/*green*/
.box { background-color: #FFF; border: 1px solid #fff; margin: auto; padding: 30px; width: auto !important; }
.mobile-logo { width: 44% !important; float: left !important; }
.mobile-header { font-size: 11px; padding-top: 10px !important; float: left !important; width: 50% !important; }
.mobile-pad-l { padding-left: 10px; }
.mobile-pad-l-2 { padding-left: 15px; }
.mobile-pad-l-3 { padding-left: 4px; }
.mobile-pad-l-4 { padding-left: 25px; }
.mobile-pad-r { padding-right: 0px; }
.mobile-width { width: 100%!important; }
.mobile-width2 { width: 69%!important; }
.mobile-pad-none { padding: 0px !important; }
.mobile-input { width: 39% !important }
.mobile-input-code { width: 100% !important }
.mob-clear { clear: both; }
hr { margin-bottom: 12px; margin-top: 20px; }
.btn-update { padding: 15px 20px !important; }
.btn-cancel { padding: 15px 20px !important; }
.ui-datepicker { display: none; padding: 0.2em 0.2em 0; width: auto !important; }
.mobile-title-width { width: 23%; }
}





/*iPhone 6 in portrait & landscape*/
@media only screen and (min-device-width : 375px) and (max-device-width : 667px) {
body { /*background-color: #FF0;  height:100vh;*/ }/*yellow*/
}





/*iPad mini in portrait & landscape*/
@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (-webkit-min-device-pixel-ratio: 1) {
body { /*background-color: #F39;  height:100vh;*/ }/*pink*/
}




/*iPad 3 & 4 Media Queries
Retina iPad in portrait & landscape*/
@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (-webkit-min-device-pixel-ratio: 2) {
body { /*background-color: #30F;  height:100vh;*/ }/*BLUE*/
}
</style>


 
</head>
<body>

<?php 
//	if ($doctors) {
//		$items = array();
//		$items[''] = '-- select --';
//		foreach ($doctors as $doctor)
//		{	
//		    $items[$doctor->DoctorID] = $doctor->DocName;
//		}
//	} else {
//		$items = array();
//		$items[''] = '-- select --';
//	}
	
        if ($procedure) {
		$items = array();
		$items[''] = '-- Select --';
		foreach ($procedure as $proc)
		{	
		    $items[$proc->ProcedureID] = $proc->Name;
		}
	} else {
		$items = array();
		$items[''] = '-- Select --';
	}

 ?>


<input type="hidden" id="h-clinicID" value="{{$clincID}}">
<input type="hidden" id="h-clinic-email" value="{{$clincID}}">
<input type="hidden" id="h-procedureID" value="0">
<input type="hidden" id="h-otp-status" value="0">

  	<div class="row">
	  	<div class=" mobile-pad-none col-xs-12 col-md-6 col-md-offset-3 text-center">
		  	<div class="box col-xs-12" id="screen1" style="display: non">
		  	<div class="mobile-width "  style=" width:510px; text-align:center; margin-left:auto; margin-right:auto;">
		  		<img class="mobile-logo"  src="{{ URL::asset('assets/images/mc-v2-logo.png') }}"   alt="" style="border-right: 1px solid #ECECEC; float:left;" />&nbsp;
                <div class="mobile-header" style=" float:left; padding-left:10px; padding-top:20px;"  >{{ $clincname }}</div><br>
		  	</div>
            <div class="clear"></div>
            <br>
		  		<form class="form-horizontal" id="form-1">

                                    
                                    <div class="form-group">
				    <label for="procedure" class="col-sm-2 col-xs-2 control-label custom-padding-r mobile-pad-l-2">Procedure</label>
				    <div class="col-sm-8 col-xs-11" >
				      {{ Form::select('procedure', $items, null, array('class' => 'form-control input-sm', 'id'=>'procedure')) }}
				    </div>
				  </div>
				  <br>
                                  
                                  
				  <div class="form-group">
				    <label for="doctor" class="col-sm-2 col-xs-2 control-label custom-padding-r mobile-pad-l">Doctor</label>
                    
				    <div class="col-sm-8 col-xs-11" >
				      {{ Form::select('doctor', array('' => '-- Select --'), null, array('class' => 'form-control input-sm', 'id'=>'doctor')) }}
				    </div>
				  </div>
				  <br>
                  
                  
                  
                  
				  
                  
                  
                  
                  
				  <div class="form-group">
				    <label for="date" class="col-sm-2 col-xs-2 control-label custom-padding-r mobile-pad-l-3">Date</label>
				    <div class="col-sm-8" >
				      {{ Form::text('date', null, array('class' => 'form-control input-sm', 'id'=>'date')) }}
				      <!-- <input type="button" class="form-control input-sm" id="date" value="" placeholder=""> -->
				    </div>
				  </div>
				  <br>
                  
                  
				  <div class="form-group ">
				    <label for="time" class="col-sm-2 col-xs-2 control-label custom-padding-r mobile-pad-l-3">Time</label>
                    <div class="mob-clear"></div>
				    <div class="col-sm-2 col-xs-5 mobile-input " >
				      <!-- {{ Form::text('time', null, array('class' => 'form-control input-sm ', 'id'=>'time')) }} -->
				      <input type="button" class="form-control input-sm" id="time" value="" placeholder="">
				    </div>
                    <div class="custom-float-l custom-padding-t">-</div>
<!-- <div class="col-sm-1 " >
				      {{ Form::text('time', '  -', array('class' => 'form-control input-sm', 'id'=>'time', '	style'=>"text-align:center !important; background:white !important")) }}
				    </div> -->
				    <!-- <span class="col-sm-1 control-label" style="text-align: center !important">-</span> -->
				    <div class="col-sm-2 col-xs-5 mobile-input" >
				      {{ Form::text('lbl_time', null, array('class' => 'form-control input-sm ', 'id'=>'lbl_time' ,'readonly','style'=>" background:white !important")) }}
				    </div>
				    <!-- <span for="lbl_time" id="lbl_time" class="col-sm-2 control-label" style="text-align: left !important"></span> -->
				  </div>

				  	<br>
				  <div class="form-group custom-margin-b">
				    <label for="remarks" class="col-sm-2 col-xs-2 control-label custom-padding-r mobile-pad-l-2">Remarks</label>
				    <div class="col-sm-8" >
				      {{ Form::text('remarks',null, array('class' => 'form-control input-sm', 'id'=>'remarks')) }}
				    </div>
				  </div>
                 
<hr>		  		
				  <div class="form-group">
				    <!--<label for="remarks" class="col-sm-2 control-label custom-padding-r"></label>-->
				    <div class="col-sm-8" >
				      <button class="col-sm-3  btn-update font-type-Montserrat" type="submit" id="btn-next">Next</button>
				      	<button class="col-sm-offset-1 col-sm-3 btn-cancel font-type-Montserrat custom-margin-l"  id="btn-cancel1" onclick="window.close();">Cancel</button>
				    </div>
				  </div>
				  <!-- <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button class="col-sm-3  btn-update font-type-Montserrat" type="submit" id="btn-next">Next</button>
				   	<button class="col-sm-offset-1 col-sm-3 btn-cancel font-type-Montserrat"  id="btn-cancel1">Cancel</button>
				    </div>
				  </div> -->
				</form>

		  	</div>	<!-- //end of 1st screeen -->
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            


		<!-- ................................................................................ -->
		  	
            
            
            
            
            
            
            
            
         	<div class="box col-xs-12" id="screen2" style="display:none"> <!-- //end of 2nd screeen -->
		  	<div class="mobile-width "  style=" width:510px; text-align:center; margin-left:auto; margin-right:auto;">
		  		<img class="mobile-logo"  src="{{ URL::asset('assets/images/mc-v2-logo.png') }}"   alt="" style="border-right: 1px solid #ECECEC; float:left;" />&nbsp;
                <div class="mobile-header" style=" float:left; padding-left:10px; padding-top:20px;"  >{{ $clincname }}</div><br>
		  	</div>
            <div class="clear"></div>
            <br>
		  		<form class="form-horizontal" id="form-2">

				  <div class="form-group">
				    <label for="doctor" class="col-sm-2 col-xs-2 control-label custom-padding-r  ">NRIC/FIN</label>
                    
				    <div class="col-sm-8 col-xs-11" >
				      {{ Form::text('nric',null, array('class' => 'form-control input-sm', 'id'=>'nric')) }}
				    </div>
				  </div>
				  <br>
                  
                  
                  
                  
				  <div class="form-group">
				    <label for="procedure" class="col-sm-2 col-xs-2 control-label custom-padding-r mobile-title-width ">Your Name</label>
				    <div class="col-sm-8 col-xs-11" >
				      {{ Form::text('name',null, array('class' => 'form-control input-sm','id'=>'name')) }}
				    </div>
				  </div>
				  <br>
                  
                  
                  
                  
				  <div class="form-group">
				    <label for="date" class="col-sm-2 col-xs-2 control-label custom-padding-r mobile-pad-l ">Phone</label>
                    <div class="mob-clear"></div>
				    <div class="col-sm-2 col-xs-2 mobile-pad-r" >
				      {{ Form::text('phone_code',null, array('class' => 'form-control input-sm mobile-input-code' ,'id'=>'phone_code','maxlength'=>"3")) }}
				    </div>
                    
                     <div class="col-sm-5 col-xs-4 mobile-width2" >
				      {{ Form::text('phone',null, array('class' => 'form-control input-sm','id'=>'phone')) }}
				    </div>
				  </div>
				  <br>
                  
                  
				  
                  <div class="form-group ">
				    <label for="time" class="col-sm-2 col-xs-2 control-label custom-padding-r mobile-pad-l-3 ">Email</label>
                    <div class="mob-clear"></div>
				    <div class="col-sm-8 col-xs-11" >
				     {{ Form::text('email',null, array('class' => 'form-control input-sm','id'=>'email')) }}
				    </div>
                   </div>

				  	<br>
                    
				 
                 
<hr>		  		
				  <div class="form-group">
				    <!--<label for="remarks" class="col-sm-2 control-label custom-padding-r"></label>-->
				    <div class="col-sm-8" >
				      <button class="col-sm-3  btn-update font-type-Montserrat" type="submit" id="btn-next">Next</button>
				      	<button class="col-sm-offset-1 col-sm-3 btn-cancel font-type-Montserrat custom-margin-l"  id="btn-cancel1" onclick="window.close();">Cancel</button>
				    </div>
				  </div>
				  <!-- <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button class="col-sm-3  btn-update font-type-Montserrat" type="submit" id="btn-next">Next</button>
				   	<button class="col-sm-offset-1 col-sm-3 btn-cancel font-type-Montserrat"  id="btn-cancel1">Cancel</button>
				    </div>
				  </div> -->
				</form>

		  	</div>	<!-- //end of 1st screeen -->
            
       
       
       
       
       
       
       
       
       
       
       
       
       
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            



















































































		<!-- ......................................................................................../ -->
		  	<div class="box" id="screen3" style="display:none"> <!-- //end of 3rd screeen -->

		  	<div class="row">
		  		<div class="col-md-12">
		  		     <div class="mobile-width "  style=" width:510px; text-align:center; margin-left:auto; margin-right:auto;">
		  		<img class="mobile-logo"  src="{{ URL::asset('assets/images/mc-v2-logo.png') }}"   alt="" style="border-right: 1px solid #ECECEC; float:left;" />&nbsp;
                <div class="mobile-header" style=" float:left; padding-left:10px; padding-top:20px;"  >{{ $clincname }}</div><br>
		  	</div>	
		  		</div>
		  	</div>
            
            
            
            
            
       
            
            
           

		  	<div class="row text-left">
		  		<br>
		  		<div class="col-md-3 col-md-offset-1">
			  		<span>&nbsp;&nbsp;Confirm Booking</span>
		  		</div>
              <div class="clear"></div>
			<hr class="custom-margin-t0">
		  	</div>
		  	<div class="row text-left">
		  		<div  class="col-md-11 col-md-offset-1">
		  			<table style="margin-bottom: auto !important" class="table borderless">
		  			<tr class="text-left">
		  				<td ><span class="field-name">Doctor</span> <p id="sc3-doctor"> </p></td>
		  				<td ><span class="field-name">NRIC</span> <p id="sc3-nric"> </p></td>
		  			</tr>
		  			<tr class="text-left">
		  				<td ><span class="field-name">Procedure</span> <p id="sc3-procedure"> </p></td>
		  				<td ><span class="field-name">Name</span> <p id="sc3-name"> </p></td>
		  			</tr>
		  			<tr class="text-left">
		  				<td ><span class="field-name">Date & Time</span> <p id="sc3-datetime"> </p></td>
		  				<td ><span class="field-name">Email and Phone</span> <p id="sc3-emailphone"></p></td>
		  			</tr>
		  			<tr class="text-left">
		  				<td ><span class="field-name">Notes</span> <p id="sc3-notes"></p></td>
		  				<td ><span class="field-name">Price</span> <p id="sc3-price"></p></td>
		  			</tr>
		  		</table>		  		
		  		</div>
		  	</div>

			<hr > 
			<div class="row text-left">
				<div class="col-md-7 col-md-offset-1">
					<form class="form-inline">
					  <div class="form-group">
					    <span class="field-name">CODE </span>
					    {{ Form::text('code',null, array('class' => 'form-control', 'id'=>'code')) }}
					    &nbsp;<span id="lbl_otp_code_msg"></span>	
					  </div>
					</form>		
				</div>
			</div>	
			<hr> 
			<div class="row text-left">
				<div class="col-md-10 col-md-offset-1">
					<form class="form-inline">
					  <div class="form-group">
					    <input type="checkbox" value="0" name="chk-condition" id="chk-condition">
					    <label style="vertical-align: bottom !important" for="chk-condition" class="field-name mobile-pad-l-4"><span><span></span></span>I agree to the <a href="https://medicloud.sg/?page_id=4234" target="_new" title="">terms and conditions</a> of <a href="https://medicloud.sg" target="_new" title="">medicloud.sg</a></label>
					  </div>
					</form>		
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12 col-md-offset-1">
					<button class="col-sm-3 btn-update font-type-Montserrat" type="submit" id="btn-confirm">Confirm Booking</button>
					<button class="col-sm-offset-1 col-sm-3 btn-cancel font-type-Montserrat custom-margin-l"  id="btn-cancel3" >Cancel</button>
				</div>
			</div>

				</div>

				
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                <!-- /Thank you page/ -->
				<div class="box" id="screen4" style="display:none">
					<div class="row">
				  		<div class="col-md-12"> 
				  			<div class="mobile-width "  style=" width:510px; text-align:center; margin-left:auto; margin-right:auto;">
		  		<img class="mobile-logo"  src="{{ URL::asset('assets/images/mc-v2-logo.png') }}"   alt="" style="border-right: 1px solid #ECECEC; float:left;" />&nbsp;
                <div class="mobile-header" style=" float:left; padding-left:10px; padding-top:20px;"  >{{ $clincname }}</div><br>
		  	</div>	
				  			</div>
				  		</div>
				  	
					<br><br>
				  	<div>
				  		<span class="font-type-Montserrat">Thank you for your booking,<br>
				  		We have confirmed your booking and dispached an email <br>
				  		to your inbox, along with  sms notification <br>
				  		<br><br>
				  		we will send you timely reminders before your <br>
				  		appointment time.</span>
				  	</div>
				  	<br><br>
				  	<div class="row mobile-pad-l-4">
				  		<button class="col-sm-offset-5 col-sm-2 btn-update font-type-Montserrat  "  id="" onclick="window.close();">Close</button>
				  	</div>

				</div> <!-- //end of screen 4 --> 

<!-- .............................................. -->

		  	</div>
		</div>
	</div>  	
</div>



</body>
</html>
