<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::get('/','App_AuthController@MainLogin');
Route::get('test','TestController@index');
Route::get('gcal/insertEvent','GoogleCalenderController@insertEvent');
Route::get('app/gcal/sendOAuthRequest','GoogleCalenderController@sendOAuthRequest');
Route::get('app/gcal/getClientToken','GoogleCalenderController@getClientToken');


Route::post('app/gcal/sendOAuthRequest','GoogleCalenderController@sendOAuthRequest');
Route::post('app/gcal/revokeToken','GoogleCalenderController@revokeToken');
Route::post('app/gcal/loadTokendGmail','GoogleCalenderController@loadTokendGmail');
Route::post('app/gcal/checkUniqueGmail','GoogleCalenderController@checkUniqueGmail');

////////// nhr   2016-2-16  for widget
Route::get('app/widget/{id}','DoctorWidgetController@index');
Route::post('app/widget/load-doctor-procedure','DoctorWidgetController@loadDoctorProcedure');
Route::post('app/widget/load-procedure-doctor','DoctorWidgetController@loadProcedureDoctor');
Route::post('app/widget/load-end-time','DoctorWidgetController@loadEndTime');
Route::post('app/widget/load-procedure-data','DoctorWidgetController@loadProcedureData');
Route::post('app/widget/new-widget-booking','DoctorWidgetController@newBooking');
Route::post('app/widget/enable-dates','DoctorWidgetController@enableDates');
Route::post('app/widget/disable-times','DoctorWidgetController@disableTimes');
Route::post('app/widget/send-otp-sms','DoctorWidgetController@sendOtpSms');
Route::post('app/widget/validate-otp','DoctorWidgetController@validateOtp');


Route::group(array('prefix' => 'admin'), function()
{
    Route::get('auth/test','Admin_AuthController@test');
});



Route::group(array('prefix' => 'v1'), function()
{
 	Route::group(array('before'=>'oauth'),function(){
 		Route::get('auth/create', 'Api_V1_AuthController@create');
 	});
	Route::post('auth/signup', 'Api_V1_AuthController@Signup');
	Route::post('auth/login','Api_V1_AuthController@Login');
        //Route::post('auth/login','Api_V1_AuthController@login');
        Route::post('auth/forgotpassword','Api_V1_AuthController@Forgot_Password');
        Route::post('auth/checkemail','Api_V1_AuthController@Check_Email');
        
        Route::post('auth/newallergy','Api_V1_AuthController@AddNewAllergy'); 
        Route::post('auth/newcondition','Api_V1_AuthController@AddNewMedicalCondition'); 
        Route::post('auth/newmedication','Api_V1_AuthController@AddNewUserMedication');
        Route::post('auth/newhistory','Api_V1_AuthController@AddNewMedicalHistory');
        Route::post('auth/update','Api_V1_AuthController@UpdateUserProfile');
        Route::post('auth/updatehistory','Api_V1_AuthController@UpdateMedicalHistory');
        Route::post('auth/change-password','Api_V1_AuthController@ChangePassword');
        Route::post('auth/device-token','Api_V1_AuthController@AddDeviceToken');
        Route::post('auth/disable-profile','Api_V1_AuthController@DisableProfile');
        Route::post('auth/otpupdate','Api_V1_AuthController@OTPProfileUpdate');
        Route::post('auth/otpvalidation','Api_V1_AuthController@OTPCodeValidation');
        Route::post('auth/otpresend','Api_V1_AuthController@OTPCodeResend');
        
        //for get
        Route::get('auth/logout','Api_V1_AuthController@LogOut');
        Route::get('auth/coordinate','Api_V1_AuthController@FindCoordinate');
        Route::get('auth/userprofile','Api_V1_AuthController@UserProfile');
        Route::get('auth/deleteallergy','Api_V1_AuthController@DeleteUserAllergy');
        Route::get('auth/deletecondition','Api_V1_AuthController@DeleteUserCondition');
        Route::get('auth/deletemedication','Api_V1_AuthController@DeleteUserMedication');
        Route::get('auth/deletehistory','Api_V1_AuthController@DeleteMedicalHistory');
        
        
        //Route::get('auth/create','Api_V1_AuthController@create');
        //        array('names' => array('create' => 'photo.build')));
        
        
        //clinic
        Route::get('clinic/search','Api_V1_ClinicController@Search');
        //Route::get('clinic/clinicdetails','Api_V1_ClinicController@ClinicDetails');
        Route::get('clinic/clinicdetails/{id}','Api_V1_ClinicController@ClinicDetails');
	Route::get('clinic/nearby','Api_V1_ClinicController@Nearby');
        Route::get('clinic/booking-history','Api_V1_ClinicController@AppointmentHistory');
        //Route::get('clinic/booking-detail','Api_V1_ClinicController@AppointmentDetails');
        Route::get('clinic/booking-detail/{id}','Api_V1_ClinicController@AppointmentDetails');
        Route::get('clinic/booking-delete','Api_V1_ClinicController@AppointmentDelete');
        Route::get('clinic/panel-nearby','Api_V1_ClinicController@PanelClinicNearby');
        Route::get('clinic/appointment','Api_V1_ClinicController@UserAppointmentValidation');
        Route::get('clinic/procedure_details/{id}','Api_V1_ClinicController@ProcedureDetails');
        Route::get('clinic/doctor_procedure/{id}','Api_V1_ClinicController@ClinicDoctorProcedures');
        
        
        //Post - Clinics
        
        
        
        
        Route::get('clinic/see','Api_V1_ClinicController@see');
        Route::post('clinic/post','Api_V1_ClinicController@post');
        
        //Route::get('doctor/doctordetails','Api_V1_DoctorController@DoctorDetails');
        //Route::get('doctor/doctor_details/{clinicid}/{doctorid}/{procedureid}','Api_V1_DoctorController@FullDoctorDetails');
        Route::post('doctor/doctor_details','Api_V1_DoctorController@FullDoctorDetails');
        Route::post('doctor/slots-refresh','Api_V1_DoctorController@AccessMoreSlots');
        Route::get('doctor/availability','Api_V1_DoctorController@getDoctorAvailableSlots');
        
        Route::get('doctor/test','Api_V1_DoctorController@test');
        
        
        //For doctor 
        Route::post('doctor/booking-queue','Api_V1_DoctorController@QueueBooking');
        Route::post('doctor/booking-slot','Api_V1_DoctorController@SlotsBooking');
        //Route::post('doctor/refresh-queue','Api_V1_DoctorController@QueueRefresh');
        //Route::post('doctor/refresh-slot','Api_V1_DoctorController@SlotRefresh');
        Route::post('doctor/moreslots','Api_V1_DoctorController@DoctorSlotsForDate');
        Route::post('doctor/morequeues','Api_V1_DoctorController@DoctorQueueForDate');
        
        Route::post('doctor/confirm-queue','Api_V1_DoctorController@ConfirmQueueBooking');
        Route::post('doctor/confirm-slot','Api_V1_DoctorController@ConfirmSlotBooking');
        Route::post('doctor/booking-delete','Api_V1_DoctorController@BookingDelete');
        
	
	//Route::resource('auth', 'Api_V1_AuthController');
        
        //For insurance company
        //GET
        Route::get('insurance/company','Api_V1_InsuranceController@getAllInsuranceCompany');
        Route::get('insurance/policy','Api_V1_InsuranceController@AllUserInsurancePolicy');
        Route::get('insurance/delete','Api_V1_InsuranceController@DeleteInsurancePolicy');
        Route::get('insurance/change-primary','Api_V1_InsuranceController@ChangePrimaryPolicy');
        
        //POST
        Route::post('insurance/add_policy','Api_V1_InsuranceController@AddUserInsurancePolicy');
        
});

//Route::group(array('domain' => 'www.tag.loc','prefix' => ''), function()
Route::group(array('prefix' => 'app'), function()
{
    Route::get('auth/getme','App_AuthController@getmenow');
    //xxxxxxxxxxxxx     For Authentication  xxxxxxxxxxxx 
    //Auth : GET
    Route::get('auth/register','App_AuthController@CompleteDoctorRegistration');
    Route::get('auth/login','App_AuthController@MainLogin');
    Route::get('auth/logout','App_AuthController@LogOutNow');
    Route::get('auth/forgot','App_AuthController@ForgotPassword');
    Route::get('auth/password-reset','App_AuthController@ResetPassword');
        //Test Route for werb Here
        //Route::get('auth/file-upload','App_AuthController@FileUpload');
        //Route::any('auth/upload','App_AuthController@Upload');
        //Route::get('auth/push','App_AuthController@PushNotification');
        
        //Route::get('auth/testshow_upload','App_AuthController@TestShowFileUpload');
        //Route::post('auth/test_upload','App_AuthController@TestUpload');
        
        
    //Auth : POST
    Route::POST('auth/signup','App_AuthController@MainSignUp');
    Route::POST('auth/loginnow','App_AuthController@ProcessLogin');
    Route::POST('auth/forgot-password','App_AuthController@ProcessForgotPassword');
    Route::POST('auth/resetpassword','App_AuthController@ProcessResetPassword');
    Route::POST('auth/find-user','App_AuthController@FindNricUser');
    
    //This is for testing purpose 
    //Route::get('auth/emailtest','App_AuthController@sendEmail');
    Route::get('auth/otptest','App_AuthController@sendOTP');
    Route::get('auth/report','App_AuthController@GetReports');
    
    
    
    
    //xxxxxxxxxxxxxx    For Clinic Areas    xxxxxxxxxxxxxx
    //Clinic : GET
    //Route::get('clinic/clinic-settings','App_ClinicController@ClinicSettings');
    Route::get('clinic/manage-doctors','App_ClinicController@ManageDoctors');
    Route::get('clinic/new-doctor','App_ClinicController@AddDoctorToClinic');
    Route::get('clinic/dashboard','App_ClinicController@ClinicDashboard');
    Route::get('clinic/booking','App_ClinicController@BookingPage');
    
    Route::get('clinic/appointment','App_ClinicController@BookNewAppointment');
    Route::get('clinic/settings','App_ClinicController@ClinicSettings');
    
    Route::get('clinic/settings-dashboard','App_ClinicController@ClinicSettingDashboard');
    Route::get('clinic/dashboard-booking/{id}','App_ClinicController@ClinicDashboardBooking');
    
    Route::get('clinic/calendar-integration','App_ClinicController@CalendarIntegrationViewPage');
    Route::get('clinic/clinic-details','App_ClinicController@ClinicDetailsPage');
    Route::get('clinic/clinic-procedure','App_ClinicController@ClinicAddProcedurePage');
    Route::get('clinic/clinic-doctor','App_ClinicController@ClinicAddDoctorPage');
    Route::get('clinic/clinic-doctors-view','App_ClinicController@ClinicDoctorsViewPage');
    Route::get('clinic/clinic-doctors-home','App_ClinicController@ClinicDoctorsHomePage');
    Route::get('clinic/update-password-home','App_ClinicController@ClinicUpdatePasswordPage');

    Route::get('clinic/calendar-integration','App_ClinicController@CalendarIntegrationViewPage');
    Route::get('clinic/button-integration','App_ClinicController@buttonIntegrationViewPage');

    Route::get('clinic/opening-times-home','App_ClinicController@ClinicOpeningTimesPage');
    Route::get('clinic/doctor-availability','App_ClinicController@ClinicDoctorAvailabilityPage');
    Route::get('clinic/appointment-home-view','App_ClinicController@ClinicHomeAppointmentPage');
    Route::get('clinic/appointment-doctor-view/{id}','App_ClinicController@SingleDoctorAppointmentPage');
    
    
    Route::get('clinic/test','App_ClinicController@test');
    Route::get('clinic/ajax','App_ClinicController@ajax');
    
    //Clinic : POST
    Route::post('clinic/ajaxpost','App_ClinicController@ajaxpost');
    Route::post('clinic/clinicdata','App_ClinicController@ClinicData');
    Route::post('clinic/load-booking','App_ClinicController@AjaxBookingPage');
    
    Route::post('clinic/ajax-settings-dashboard','App_ClinicController@ClinicSettingDashboardAJAX');
    Route::post('clinic/ajax-dashboard-booking','App_ClinicController@ClinicDashboardBookingAJAX');
    
    Route::post('clinic/clinic-image-upload','App_ClinicController@ClinicProfileImageUpload');
    Route::post('clinic/clinic-profile-update','App_ClinicController@ClinicDetailsPageUpdate');
    
    Route::post('clinic/add-procedure','App_ClinicController@ClinicAddProcedure');
    Route::post('clinic/delete-procedure','App_ClinicController@ClinicDeleteProcedure');
    Route::post('clinic/add-doctor','App_ClinicController@ClinicAddDoctors');
    Route::post('clinic/delete-doctor','App_ClinicController@ClinicDoctorsDelete');
    Route::post('clinic/update-password','App_ClinicController@ClinicPasswordUpdate');
    Route::post('clinic/opening-times','App_ClinicController@ClinicOpeningTimes');
    Route::post('clinic/delete-opening-times','App_ClinicController@ClinicDeleteOpeningTimes');
    Route::post('clinic/clinic-holidays','App_ClinicController@AddClinicHolidays');
    Route::post('clinic/delete-clinic-holiday','App_ClinicController@DeleteClinicHolidays');
    Route::post('clinic/load-doctor-availability','App_ClinicController@LoadClinicDoctorAvailabilityPage');
    Route::post('clinic/availability-times','App_ClinicController@AddDoctorAvailabilityTimes');
    Route::post('clinic/repeat-action','App_ClinicController@RepeatTimeActions');
    Route::post('clinic/open-booking-page','App_ClinicController@OpenBookingPage');
    Route::post('clinic/open-booking-update','App_ClinicController@OpenBookingUpdate');
    Route::post('clinic/load-booking-popup','App_ClinicController@LoadBookingPopup');
    
    Route::post('clinic/doctor-procedures','App_ClinicController@LoadDoctorProcedures');
    Route::post('clinic/new-appointment','App_ClinicController@NewClinicAppointment');
    Route::post('clinic/update-appointment','App_ClinicController@UpdateClinicAppointment');
    Route::post('clinic/delete-appointment','App_ClinicController@DeleteClinicAppointment');
    Route::post('clinic/conclude-appointment','App_ClinicController@ConcludeClinicAppointment');
    Route::post('clinic/load-appointment-view','App_ClinicController@LoadDoctorsAppointmentView');
    
    Route::post('clinic/load-doctors-view','App_ClinicController@LoadDoctorsSelectionView');
    Route::post('clinic/load-singledoctor-view','App_ClinicController@LoadSingleDoctorView');
    Route::post('clinic/change-procedure','App_ClinicController@ChangeProcedures');
    Route::post('clinic/change-startdate','App_ClinicController@ChangeStartDate');
    Route::get('clinic/doctor-update-page/{id}','App_ClinicController@UpdateDoctorPage');
    Route::post('clinic/update-doctor','App_ClinicController@UpdateDoctorDetails');
    Route::post('clinic/channel_update','App_ClinicController@UpdateBookingChannel');
    
            
            
    //Route::get('clinic/','App_ClinicController@index');
    //Route::get('auth/create', 'App_AuthController@create');
	Route::resource('auth', 'App_AuthController');
        
    //xxxxxxxxxxx       For Doctor Area         xxxxxxxxxxxxxxxx
    //Doctor    :   POST
    Route::post('doctor/manageslots','App_DoctorController@ManageDoctorSlots');    
    Route::post('doctor/newdoctor','App_DoctorController@AddNewDoctor');
    Route::post('doctor/updatedoctor','App_DoctorController@UpdateDoctor');
    Route::post('doctor/manage-slotdetail','App_DoctorController@ManageSlotDetails');
    Route::post('doctor/manage-slotdate','App_DoctorController@ManageSlotsForDate');
    
    Route::post('doctor/booking-queue','App_DoctorController@QueueSlotBooking');
    Route::post('doctor/delete-appointment','App_DoctorController@DeleteUserAppointment');
    Route::post('doctor/delete-popup','App_DoctorController@OpenDeleteAppointment');
    Route::post('doctor/manage-queue','App_DoctorController@StoppedDoctorQueue');
    Route::post('doctor/start-queue','App_DoctorController@StartedDoctorQueue');
    Route::post('doctor/booking','App_DoctorController@DoctorBooking');
    Route::post('doctor/ajax-booking','App_DoctorController@AjaxDoctorBooking');
    Route::post('doctor/diagnosis','App_DoctorController@DoctorDiagnosis');
    
    
    
    
    
    
    //Doctor    :   GET
    Route::get('doctor/settings','App_DoctorController@DoctorSettings'); 
    Route::get('doctor/dashboard','App_DoctorController@doctorDashboard');
    Route::get('doctor/home','App_DoctorController@DoctorHome');
    
        
    //Cron Jobs Implementation 
    //Route::get('cron/test','CronController@CronTest');
    Route::get('cron/reminder-today','CronController@RemindAppointment');
    Route::get('cron/reminder-hours','CronController@RemindAppointmentInHours');
    Route::get('cron/reminder-minutes','CronController@RemindAppointmentInMinutes');
    Route::get('cron/diactivate_booking','CronController@DiactivateBookings');
    
    Route::get('cron/reminder-tomorrow','CronController@SMSAppointmentBeforeDay');
    Route::get('cron/reminder-hour','CronController@SMSAppointmentBeforeHour');
    
    Route::get('cron/delete-googlevents','CronController@deleteGoogleEvent');
        //Route::get('doctor/file-upload','App_DoctorController@DoctorUpload');
        //Route::any('doctor/upload','App_DoctorController@Upload');
        
});

Route::post('oauth/access_token', function() {
	//return Response::json();
});


/********************Admin Dashboard*********************************/
    Route::group(array('prefix' => 'admin'), function(){   

    ######################Admin_ClinicController#######################
    
    //Admin Login
        Route::get('auth/login','Admin_AuthController@MainLogin');
        Route::post('auth/loginprocess','Admin_AuthController@LoginProcess');
        Route::get('auth/logout','Admin_AuthController@LogOutNow');
        
    //add clinic view
    Route::get('clinic/new-clinic','Admin_ClinicController@AddNewClinic');
    //show all clinic view
    //Route::get('clinic/all-clinics','Admin_ClinicController@ShowAllClinics');
    //show all clinic view
    Route::get('clinic/get-all-clinics','Admin_ClinicController@showAllClinic');

    Route::get('clinic/all-clinics','Admin_ClinicController@getAllClinics');

    //save clinic 
    Route::post('clinic/insert-clinic', array('uses'=>'Admin_ClinicController@InsertClinic'));
    //edit clinic details
    Route::get('clinic/{id}/edit', 'Admin_ClinicController@EditClinic');
    //update clinic details
    Route::post('clinic/{id}/update', 'Admin_ClinicController@UpdateClinic');

    ####################################################################

    ######################Admin_DoctorController########################

    //add doctor view
    Route::get('clinic/new-doctor', 'Admin_DoctorController@AddNewDoctor');
    //save doctor details 
    Route::post('clinic/insert-doctor', array('uses'=>'Admin_DoctorController@InsertDoctor'));

    //show all doctors view
    Route::get('clinic/all-doctors','Admin_DoctorController@ShowAllDoctors');
    //edit doctor details
    Route::get('clinic/doctor/{id}/edit', 'Admin_DoctorController@EditDoctor');
    //update doctor details
    Route::post('clinic/doctor/{id}/update', 'Admin_DoctorController@UpdateDoctor');
    
    ####################################################################
    

    // Route::get('clinic/sendemail','Admin_AuthController@SendEmail');
    
    //add clinic time view
    Route::get('clinic/{id}/new-time','Admin_ClinicController@AddClinicTime');
    //save clinic time details
    Route::post('clinic/insert-time', array('uses'=>'Admin_ClinicController@InsertClinicTimeDetails'));
    //edit clinic time details
    Route::get('clinic/time/{id}/edit', 'Admin_ClinicController@EditTimeSchedule');
    //update clinic time details
    Route::post('clinic/time/{id}/update', 'Admin_ClinicController@UpdateTimeSchedule');
    
    Route::get('clinic/search_booking', 'Admin_ClinicController@SearchBookingPage');
    Route::post('clinic/search_booking_process', 'Admin_ClinicController@SearchBooking');
    Route::post('clinic/clinic_doctor', 'Admin_ClinicController@ClinicDoctor');
});