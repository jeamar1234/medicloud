//window.base_url = 'http://localhost/medicloud_web/public/app/';
//window.base_url = 'http://medicloud.sg/medicloud_web/public/app/';


window.base_loading_image = '<img src="https://medicloud.sg/medicloud_web/public/assets/images/ajax-loader.gif" width="32" height="32"  alt=""/>';

jQuery("document").ready(function(){
	var protocol = jQuery(location).attr('protocol');
	var hostname = jQuery(location).attr('hostname');
	var folderlocation = $(location).attr('pathname').split('/')[1];
	window.base_url = protocol+'//'+hostname+'/'+folderlocation+'/public/app/';

//    window.base_url = 'http://'+jQuery(location).attr('hostname')+'/medicloud_web/public/app/';

    //console.log(base_url);

    doctorSignUpValidation();
    resetPasswordValidation();
    
    jQuery("#Doctor-Signup").click(function(){
        var userid = jQuery(this).attr('userid');
        var email = jQuery("#Email").val();
        var password = jQuery("#Password").val();
        
        dataValues = 'userid='+userid+'&email='+email+'&password='+password;
        
        if(jQuery("#form-signup").valid() ==true){
            jQuery.blockUI({ message: '<h1> '+base_loading_image+' <br /> Please wait for a moment</h1>' });
            jQuery.ajax({
                type: "POST",
                url : base_url+"auth/signup",
                data : dataValues,
   
                success : function(data){
                    if(data ==1){
                        window.location = base_url+"doctor/dashboard";
                    }else{
                        window.location = base_url+"auth/login";
                    }
                    jQuery.unblockUI();   
                }       
            }); 
        }
    });
    
    jQuery("#auth-login").click(function(){
        var email = jQuery("#Email").val();
        var password = jQuery("#Password").val();
        
        dataValues = '&email='+email+'&password='+password;
        
        if(jQuery("#form-signup").valid() ==true){
            jQuery.blockUI({ message: '<h1> '+base_loading_image+' <br /> Please wait for a moment</h1>' });
            jQuery.ajax({
                type: "POST",
                url : base_url+"auth/loginnow",
                data : dataValues,
   
                success : function(data){
                    if(data ==2){
                        //window.location = base_url+"doctor/dashboard";
                        window.location = base_url+"doctor/home";
                    }else if(data ==3){
                        //window.location = base_url+"clinic/booking";
                        //window.location = base_url+"clinic/settings-dashboard";
                        window.location = base_url+"clinic/appointment-home-view";
                    }else{
                        jQuery("#ajax-error").html("<div class='error-notification'>Please check your login credential</div>");
                        //window.location = base_url+"auth/login";
                    }
                    jQuery.unblockUI();   
                }       
            }); 
        }
    });

    /* Use          :   Used to reset password 
     * 
     */
    jQuery("#auth-forgot").click(function(){
        var email = jQuery("#Email").val();
        dataValues = '&email='+email;
        
        if(jQuery("#form-signup").valid() ==true){
            jQuery.blockUI({ message: '<h1> '+base_loading_image+' <br /> Please wait for a moment</h1>' });
            jQuery.ajax({
                type: "POST",
                url : base_url+"auth/forgot-password",
                data : dataValues,
                success : function(data){
                    if(data==1){
                        jQuery("#ajax-error").html("<div class='error-notification'>We have sent password reset instructions to your email</div>");
                    }else{
                        jQuery("#ajax-error").html("<div class='error-notification'>Sorry ! We could not find any information</div>");
                    }
                    //console.log(data);
//                    if(data ==2){
//                        window.location = base_url+"doctor/dashboard";
//                    }else if(data ==3){
//                        window.location = base_url+"clinic/dashboard";
//                    }else{
//                        jQuery("#ajax-error").html("<div class='error'>Please check your login credential</div>");
//                        //window.location = base_url+"auth/login";
//                    }
                    jQuery.unblockUI();   
                }       
            }); 
        }
    });
    
    /* Use          :   Used to update new password
     * By           :   AJAX
     */
    jQuery("#auth-reset").click(function(){
        var oldpass = jQuery("#OldPassword").val();
        var newpass = jQuery("#Password").val();
        var userid = jQuery(this).attr('userid');
        
        dataValues = 'userid='+userid+'&oldpass='+oldpass+'&newpass='+newpass;
        
        if(jQuery("#form-reset").valid() ==true){
            jQuery.blockUI({ message: '<h1> '+base_loading_image+' <br /> Please wait for a moment</h1>' });
            jQuery.ajax({
                type: "POST",
                url : base_url+"auth/resetpassword",
                data : dataValues,
                success : function(data){
                    if(data==1){
                        jQuery("#ajax-error").html("<div class='error-notification'>Your password successfully updated</div>");
                        window.location = base_url+"auth/login";
                    }else if(data==2){
                        jQuery("#ajax-error").html("<div class='error-notification'>We could not recognized your old password</div>");
                    }else{
                        jQuery("#ajax-error").html("<div class='error-notification'>Sorry ! Please try again</div>");
                    }              
                    jQuery.unblockUI();   
                }       
            }); 
        }
    });
    
    
    
    
});
